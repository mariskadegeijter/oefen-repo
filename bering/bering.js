'use strict';

const fs = require('fs').promises;

var messages = []

function _removeAccents(strAccents) {
    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents =    "ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž";
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
        } else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');
    return strAccentsOut;
}

function _chkVal(val) {
	if(Array.isArray(val)) {
		return Boolean(val.length > 0 && val[0] !== "")
	} else {
		return Boolean(val)
	}
}

function _convertStripe(str) {
    return String(str).replace("_","")
}

function _removeATM(val) {
    return String(val).replace(" ATM","")
}

function _removeMM(val) {
    return String(val).replace(" mm","")
}

function parseJson(data) {
    try {
        var res = JSON.parse(data);
    } catch(err) {
        var res = null
    }
    return res
}
 //toLowerCase function
function parseWatchProperties(article,art,currentProductNumber,productAttributes){

    if(_chkVal(productAttributes)){
        if(productAttributes['horlogeglas'] !== undefined){
            article.watch.case.glassMaterial = getPrismaNoteProperty('caseGlasstype',productAttributes['horlogeglas'],'caseGlasstype',currentProductNumber)
        }
        if(productAttributes['uurwerk'] !== undefined){
            article.watch.movement = getPrismaNoteProperty('watchMovement',productAttributes['uurwerk'],'watchMovement',currentProductNumber)
        }
        if(productAttributes['kastkleur'] !== undefined){
            article.watch.case.color = getPrismaNoteProperty('Color',productAttributes['kastkleur'],'Color',currentProductNumber)
        }
        if(productAttributes['kastdikte'] !== undefined){
            article.watch.case.depth = _removeMM(productAttributes['kastdikte'])
        }
        if(productAttributes['kastdiameter'] !== undefined){
            article.watch.case.size = _removeMM(productAttributes['kastdiameter'])
        }   else if(productAttributes['kastbreedte'] !== undefined){
                article.watch.case.size = _removeMM(productAttributes['kastbreedte'])
            }
        if(productAttributes['kastmateriaal'] !== undefined){
            article.watch.case.material = getPrismaNoteProperty('caseMaterial',productAttributes['kastmateriaal'],'caseMaterial',currentProductNumber)
            if(article.watch.case.material === 'TITANIUM'){
                article.watch.isAntiAllergy = true
                article.watch.isNickleFree = true
            }
        }
        if(productAttributes['bandmateriaal'] !== undefined){
            article.watch.strap.material = getPrismaNoteProperty('strapMaterial',productAttributes['bandmateriaal'],'strapMaterial',currentProductNumber)
            if(article.watch.strap.material === 'TITANIUM'){
                article.watch.isAntiAllergy = true
                article.watch.isNickleFree = true
            }
        }
        if(productAttributes['bandkleur'] !== undefined){
            article.watch.strap.color = getPrismaNoteProperty('Color',productAttributes['bandkleur'],'strapMaterial',currentProductNumber)
        }
        if(productAttributes['bandbreedte'] !== undefined){
            article.watch.strap.width = _removeMM(productAttributes['bandbreedte'])
        }
        if(productAttributes['sluiting'] !== undefined){
            article.watch.strap.clasp = productAttributes['sluiting']
            if(productAttributes['sluiting'].toLowerCase().includes('butterfly')){
                article.watch.strap.clasp = "Vlindersluiting"
            }
        }
        if(productAttributes['wijzerplaatkleur'] !== undefined){
            article.watch.dial.color = getPrismaNoteProperty('Color',productAttributes['wijzerplaatkleur'],'Color',currentProductNumber)
        }
        if(productAttributes['waterdichtheid'] !== undefined){
            article.watch.waterProofLevel = _removeATM(productAttributes['waterdichtheid'])
        }
    }

    return article
}

function parseStrapProperties(article,art,currentProductNumber,productAttributes){

    if(_chkVal(productAttributes)){
        if(productAttributes['bandkleur'] !== undefined){
            article.strap.color = getPrismaNoteProperty('Color', productAttributes['bandkleur'], 'Color', currentProductNumber)
        }
        if(productAttributes['bandbreedte'] !== undefined){
            article.strap.width = _removeMM(productAttributes['bandbreedte'])
        }
        if(productAttributes['sluiting'] !== undefined){
            article.strap.clasp = productAttributes['sluiting']
            if(productAttributes['sluiting'].toLowerCase().includes('butterfly')){
                article.watch.strap.clasp = "Vlindersluiting"
            }
        }
        if(productAttributes['bandmateriaal'] !== undefined){
            article.strap.material = getPrismaNoteProperty('strapMaterial', productAttributes['bandmateriaal'],'strapMaterial',currentProductNumber)
        }
    }

    return article
}

function parseJewelProperties(article,art,currentProductNumber, productAttributes){

    if(_chkVal(productAttributes)) {
        if(productAttributes['categorie'] !== undefined){
            article.jewel.type = getPrismaNoteProperty('jewelType', productAttributes['categorie'], 'jewelType', currentProductNumber)
        }

        if(productAttributes['sieraadkleur'] !== undefined){
           article.jewel.color = getPrismaNoteProperty('Color', productAttributes['sieraadkleur'], 'Color', currentProductNumber)
        }
        if(productAttributes['sieraadmateriaal'] !== undefined){
           article.jewel.material = getPrismaNoteProperty('jewelMaterial', productAttributes['sieraadmateriaal'], 'jewelMaterial', currentProductNumber)
        }
        if(productAttributes['sieraaddiameter'] !== undefined){
            article.jewel.diameter = _removeMM(productAttributes['sieraaddiameter'])
            article.variants.size = _removeMM(productAttributes['sieraaddiameter'])
            article.size = _removeMM(productAttributes['sieraaddiameter'])
        }
        if(productAttributes['sieraadbreedte'] !== undefined){
            article.jewel.width = _removeMM(productAttributes['sieraadbreedte'])
            if(article.jewel.size === 0){
                article.variants.size = _removeMM(productAttributes['sieraadbreedte'])
                article.size = _removeMM(productAttributes['sieraadbreedte'])
            }
        }

    }

    return article
}

function getAssociatedAttributes(allAttributes, id) {
    if(_chkVal(id)) {
        var attrOfThisProduct = allAttributes.find(function(p) {
            return p.id === String(id)
        });

        if(typeof attrOfThisProduct !== 'undefined') {
            return attrOfThisProduct
        } else {
            return undefined
        }

    } else {
        return undefined
    }
}

function beringParser(srcData,availableAttributes) {

    let productsFound = []

    if(srcData.rss.channel && srcData.rss.channel.item && srcData.rss.channel.item.length > 0) {

        let articles = srcData.rss.channel.item
        let currentBrandName = "Bering"
        let currentProductNumber = ""

        if (articles && articles.length > 0) {
            articles.forEach(function(art) {
                // console.log(art)
            //article object created from blueprint
            let article = {
                "productNumber":"",
                "eanNumber":"",
                "images":[],
                "variants": [],
                "hasStock":true,
                "size":0,
                "weight":0,
                "totalCaratWeight":0,
                "jewel":{
                    "material":"",
                    "color":"",
                    "type":"",
                    "height":0,
                    "width":0,
                    "depth":0,
                    "diameter":0,
                    "weight":0,
                    "chain":"",
                    "clasp":"",
                    "shape":"",
                    "gloss":false
                },
                "strap":{
                        "model":"",
                        "width":0.0,
                        "material":"",
                        "color":"",
                        "print":"",
                        "studs":"",
                        "clasp":"",
                        "pattern":"",
                        "wristPermimeter":""
                    },
                "watch":{
                    "type":"WRIST",
                    "movement":"",
                    "indication":"",
                    "hasSwissMovement":false,
                    "hasDateFunction":false,
                    "waterProofLevel":null,
                    "isNickelFree":false,
                    "isAntiAllergy":false,
                    "hasLightFunction":false,
                    "isSmartWatch":false,
                    "smartWatchFunctions":"",
                    "dial":{
                        "color":"",
                        "pattern":"",
                        "print":"",
                        "index":""
                    },
                    "case":{
                        "shape":"",
                        "size":0,
                        "depth":0,
                        "material":"",
                        "glassMaterial":"",
                        "color":""
                    },
                    "strap":{
                        "model":"",
                        "width":0.0,
                        "material":"",
                        "color":"",
                        "print":"",
                        "studs":"",
                        "clasp":"",
                        "pattern":"",
                        "wristPermimeter":""
                    }
                },
                "category":"",
                "brand":{
                    "_id":"",
                    "name":"",
                    "alias":"",
                    "nameSlug":"",
                    "description":"",
                    "images":[],
                    "startingDigits":"",
                    "isPoolArticleBrand":false
                },
                "suggestedRetailPrice":0,
                "suggestedRetailPriceVat":0,
                "male":false,
                "female":false,
                "kids":false,
                "nl":{
                    "name":"",
                    "shortDescription":"",
                    "longDescription":""
                },
                "en":{
                    "name":"",
                    "shortDescription":"",
                    "longDescription":""
                },
                "de":{
                    "name":"",
                    "shortDescription":"",
                    "longDescription":""
                },
                "uploader": {
                    "_id": "",
                    "name": "Bering"
                }
            }

            //set productnumber
            if(_chkVal(art['g:id'])){
                if(art['g:id'].includes('_')) {
                    currentProductNumber = _convertStripe(art['g:id'])
                } else {
                    currentProductNumber = art['g:id']
                }
            } else {
                messages.push({"product":"unknown","problem":"product has no id"})
            }

            article.productNumber = currentProductNumber
            //set ean
            var currentEAN = _chkVal(art['g:gtin']) ? art['g:gtin'] : ""
            article.eanNumber = currentEAN
            //set image
            if(_chkVal(art['g:image_link'])) {
                article.images.push({
                    src: art['g:image_link'],
                    alt: _chkVal(art['g:description']) ? art['g:description'] : currentProductNumber
                })
                //if(!_chkVal(art['g:description'])){ messages.push({"product":art['g:id'],"problem":"has no description as for image alt"}) }
            } else{
                if(!_chkVal(art['g:image_link'])){ messages.push({"product":art['g:id'],"problem":"has no image url"}) }
                
            }

            //set variant
            let variant = {
                "productNumberAuto":false,
                "size":0,
                "productNumber":currentProductNumber,
                "insertedProductNumber":currentProductNumber,
                "ean": currentEAN,
                "shopStratingDigits":"" //no starting digits for Bering
            }

            article.variants.push(variant)

            //set category
            let categoryNL = ""
            let categoryEN = ""
            let categoryDE = ""

            if(_chkVal(art['g:product_type'])){

                if(_chkVal(art['g:description']) && art['g:description'].includes('wekker')){
                    article.category = 'OTHER'
                    categoryNL = 'wekker'
                    categoryEN = 'alarm clock'
                    categoryDE = 'wecker'
                } else if(art['g:product_type'].includes('horloge')) {
                    article.category = 'WATCH'
                    categoryNL = 'horloge'
                    categoryEN = 'watch'
                    categoryDE = 'uhr'
                } else if(art['g:product_type'].includes('armband') || art['g:product_type'].includes('ring') || art['g:product_type'].includes('ketting') || art['g:product_type'].includes('charm') || art['g:product_type'].includes('oorknop') || art['g:product_type'].includes('oorring')){
                    article.category = 'JEWEL'
                    categoryNL = 'sieraad'
                    categoryEN = 'jewel'
                    categoryDE = 'schmuckstuck'
                } else if(art['g:product_type'].includes('band')) {
                    article.category = 'STRAP'
                    categoryNL = 'horlogeband'
                    categoryEN = 'watch strap'
                    categoryDE = 'uhrenarmband'
                } else {
                    article.category = 'OTHER'
                }

            } else {
                messages.push({"product":"unknown","problem":"invalid product"})
                console.log('invalid', art)
            }

            if(_chkVal(art['g:description'])){
                article.nl.shortDescription = art['g:description']
            }

            //set brandinformation in Node Red
            article.brand.name = 'Bering'

            //set price
            if(_chkVal(art['g:price'])) {
                article.suggestedRetailPrice = parseFloat(art['g:price'])/1.21
                article.suggestedRetailPriceVat = 21
            }

            var productAttributes = ""
            if(art['g:item_group_id']){
                productAttributes = getAssociatedAttributes(availableAttributes, art['g:item_group_id'])
            } else if(art['g:id']){
                productAttributes = getAssociatedAttributes(availableAttributes, art['g:id'])
            } else if(art['g:id']){
                productAttributes = getAssociatedAttributes(availableAttributes, art['g:mpn'])
            }

            //set gender
            let genderNL = ""
            let genderTranslateEN = ""
            let genderTranslateDE = ""

            
            if(_chkVal(productAttributes['gender'])){
                if(productAttributes['gender'] !== undefined){
                    if(productAttributes['gender'].includes('mannelijk')){
                        article.male = true
                        article.female = false
                        genderNL = 'heren'
                        genderTranslateEN = 'gentleman'
                        genderTranslateDE = 'herren'
                    } else if(productAttributes['gender'].includes('Unisex')) {
                        article.male = true
                        article.female = true
                        genderNL = 'unisex'
                        genderTranslateEN = 'unisex'
                        genderTranslateDE = 'unisex'
                    } else if(productAttributes['gender'].includes('vrouw')){
                        article.male = false
                        article.female = true
                        genderNL = 'dames'
                        genderTranslateEN = 'ladies'
                        genderTranslateDE = 'damen'
                    } else {
                        article.male = true
                        article.female = true
                        genderNL = 'unisex'
                        genderTranslateEN = 'unisex'
                        genderTranslateDE = 'unisex' 
                    }
                }
            }
            

            //set nl, en and de
            if(_chkVal(art['title']) && categoryNL !== 'wekker'){
                article.nl.name = art['title'].toUpperCase()
            } else if(_chkVal(art['g:id'])){
                article.nl.name = ['bering',categoryNL,genderNL,art['g:id']].join(' ').toUpperCase()
            } 
            article.en.name = ['bering',categoryEN,genderTranslateEN,art['g:id']].join(' ').toUpperCase()
            article.de.name = ['bering',categoryDE,genderTranslateDE,art['g:id']].join(' ').toUpperCase()

            if(article.category === "WATCH"){
                article = parseWatchProperties(article,art,currentProductNumber,productAttributes)
                delete article.jewel
                delete article.strap
            } else if(article.category === "JEWEL"){
                article = parseJewelProperties(article,art,currentProductNumber,productAttributes)
                delete article.watch
                delete article.strap
            } else if(article.category === "STRAP"){
                article = parseStrapProperties(article,art,currentProductNumber,productAttributes)
                delete article.watch
                delete article.jewel
            } else{
                delete article.jewel
                delete article.strap
                delete article.watch 
            }

            productsFound.push(article)
            
        });
    } else {
        throw new Error("No articles in source data")
    }
    console.log('Variantencollectie en variantBaseA toevoegen')
    console.log("-- Finished --")
    console.log("Products: "+productsFound.length)

    let problems = messages.filter(m => { return m.problem && m.problem !== '' });
    let warnings = messages.filter(m => { return m.warning && m.warning !== '' });
    let notifications = messages.filter(m => { return m.notification && m.notification !== '' });

    console.log("Problems: ",problems.length)
    console.log("Warnings: ",warnings.length)
    console.log("Notifications: ",notifications.length)

    return {
        "data":productsFound,
        "problems":problems,
        "warnings":warnings,
        "notifications":notifications
    };
}
}

function getPrismaNoteProperty(type,val,srcProp,currentProductNumber=null) {

        val = _removeAccents(val).toLowerCase()
        if(![
            "caseGlasstype",
            "caseMaterial",
            "caseShape",
            "Color",
            "dialIndex",
            "dialPattern",
            "gemKind ",
            "jewelMaterial",
            "jewelType",
            "strapMaterial",
            "strapModel",
            "watchIndication",
            "watchMovement",
            "watchType"
            ].includes(type)) {
            return null
        }

        let res = {
            "success":false,
            "data":"",
            "errors":[]
        }

        let targetProp = null
        switch(type) {
            case "caseGlasstype":
                targetProp = caseGlasstype
            break;
            case "caseMaterial":
                targetProp = caseMaterial   
            break;
            case "caseShape":
                targetProp = caseShape      
            break;
            case "Color":
                targetProp = Color
            break;
            case "dialIndex":
                targetProp = dialIndex
            break;
            case "dialPattern":
                targetProp = dialPattern
            break;
            case "gemKind ":
                targetProp = gemKind    
            break;
            case "jewelMaterial":
                targetProp = jewelMaterial  
            break;
            case "jewelType":
                targetProp = jewelType
            break;
            case "jewelColor":
                targetProp = Color
            break;
            case "strapMaterial":
                targetProp = strapMaterial
            break;
            case "strapModel":
                targetProp = strapModel
            break;
            case "watchIndication":
                targetProp = watchIndication
            break;
            case "watchMovement":
                targetProp = watchMovement
            break;
            case "watchType":
                targetProp = watchType
            break;

            default:
                targetProp = null
            break;
        }

        if(_chkVal(targetProp)) {
        if(val in targetProp) { //there's an exact match
            res.success = true
            res.data = targetProp[val]
        } else { //there's no match, so let's try one of the synonyms
            beringSynonyms.forEach(s => {
                if(type === s['key'] && val === s['val']) {
                    res.success = true
                    res.data = s['equals']
                    messages.push({"product":(currentProductNumber ? currentProductNumber : "(no productnumber)"),"notification":String(type+" '"+val+"' - alias found: "+s['equals'])})
                 } 
                //else{
                //     if(type === 'Color' && type === s['key'] && val === s['val']){console.log(currentProductNumber, "key and val are not okay")}
                //     else if(type === 'Color' && val !== s['val']){ console.log(currentProductNumber, "val is not okay")}
                //     else if(type === s['key']){console.log(currentProductNumber, 'key is not okay')}
                // }
            });

            // if(!res.success) {
            //     beringArticleOverrides.forEach(s => {
            //         if(s['attr'] === type && s['of'] === srcProp && s['sku'] === currentProductNumber) {
            //             res.data = s['equals']
            //             res.success = true
            //             messages.push({"product":(currentProductNumber ? currentProductNumber : "(no productnumber)"),"notification":String(type+" '"+val+"' overwritten by "+s['equals'])})
            //         }
            //     });

                if(!res.success) {
                    console.log("Not found: ", currentProductNumber,srcProp,type)
                }
            // }
        }
    }

    if(res.success) {
        return res.data
    } else {
        messages.push({"product":(currentProductNumber ? currentProductNumber : "(no productnumber)"),"problem":String(type+" '"+val+"' not found for "+(srcProp ? srcProp : ""))})
        return;
    }

        // if(_chkVal(targetProp)) {
        //     console.log('coming in 580')
        //     if(val in targetProp) { //there's an exact match
        //         res.success = true
        //         res.data = targetProp[val]
        //         console.log('coming in 584')
        //     } else { //there's no match, so let's try one of the synonyms
        //         console.log('coming in 586')
        //         beringSynonyms.forEach(s => {
        //             if(type === s['key'] && val === s['val']) {
        //                 res.success = true
        //                 res.data = s['equals']
        //                 messages.push({"product":(currentProductNumber ? currentProductNumber : "(no productnumber)"),"notification":String(type+" '"+val+"' - alias found: "+s['equals'])})
        //             }
        //         });

        //         if(!res.success) {
        //             // console.log("Not found: ",currentProductNumber,srcProp,type)
        //         }
        //     }
        // } else(console.log('not coming '))

}

// const products = init(srcData)

// msg.payload = {
//     products: products,
//     feedName: 'Bering'
// }

// return msg;

const gemKind = {
    "agaat" : "AGATE",
    "amazone" : "AMAZONE",
    "amazoniet" : "AMAZONITE",
    "barnsteen" : "AMBER",
    "amethyst" : "AMETHYST",
    "aquamarijn" : "AQUAMARINE",
    "aventurijn" : "AVENTURINE",
    "beryl" : "BERYLL",
    "bloedkoraal" : "BLOODCORAL",
    "calciet" : "CALCITE",
    "cameo" : "CAMEO",
    "carneool" : "CARNELEAN",
    "cateye" : "CATEYE",
    "keramiek" : "CERAMICS",
    "chalcedoon" : "CHALCEDONY",
    "citrien" : "CITRINE",
    "korund" : "CORUNDUM",
    "kristal" : "CRYSTAL",
    "diamant" : "DIAMOND",
    "smaragd" : "EMERALD",
    "glazuur" : "ENAMEL",
    "epidote" : "EPIDOTE",
    "firepost" : "FIREPOST",
    "fluoriet" : "FLUORITE",
    "granaat" : "GARNET",
    "glas" : "GLASS",
    "heliotroop" : "HELIOTROPE",
    "hematiet" : "HEMATITE",
    "iriskwarts" : "IRISQUARTZ",
    "jade" : "JADE",
    "jargon" : "JARGON",
    "jasper" : "JASPER",
    "labradoriet" : "LABRADORITE",
    "lapis" : "LAPIS",
    "lazuli" : "LAZULI",
    "lazuriet" : "LAZURITE",
    "citroenkwarts" : "LEMONQUARTZ",
    "madeiracitrine" : "MADEIRACITRINE",
    "malachiet" : "MALACHITE",
    "maansteen" : "MOONSTONE",
    "muranoglass" : "MURANOGLASS",
    "natuursteen" : "NATURALSTONE",
    "nefriet" : "NEPHRITE",
    "onyx" : "ONYX",
    "opaal" : "OPAL",
    "parel" : "PEARL",
    "parel akoya" : "PEARLAKOYA",
    "parel cultive" : "PEARLCULTIVE",
    "parel zuidzee" : "PEARLSOUTHSEA",
    "parel zoetwater" : "PEARLSWEETWATER",
    "parel synthetisch" : "PEARLSYNTHETICAL",
    "parel tahiti" : "PEARLTAHITI",
    "peridoot" : "PERIDOTE",
    "fosforiet" : "PHOSPHORITE",
    "quartz" : "QUARTZ",
    "strass" : "RHINESTONE",
    "steenkristal" : "ROCKCRYSTAL",
    "rozenkwarts" : "ROSEQUARTZ",
    "robijn" : "RUBY",
    "saffier" : "SAPPHIRE",
    "leisteen" : "SLATE",
    "rookkwarts" : "SMOKYQUARTZ",
    "soladiet" : "SODALITE",
    "spinel" : "SPINEL",
    "laagsteen" : "STRATUMSTONE",
    "swarovski kristal" : "SWAROVSKICRYSTAL",
    "synthetisch" : "SYNTHETICSTONE",
    "tijgeroog" : "TIGEREYE",
    "topaas" : "TOPAZ",
    "toermalijn" : "TOURMALINE",
    "turquoise" : "TURQUOISE",
    "uvaroviet" : "UVAROVITE",
    "wildvinite" : "WILDVINITE",
    "xylopile" : "XYLOPILE",
    "zirkonia" : "ZIRCONIA"
}

const jewelType = {
    "enkelbandje" : "ANKLE_BRACELET",
    "armband" : "BRACELET",
    "broche" : "BROOCH",
    "bedel" : "CHARM",
    "choker" : "CHOKER",
    "combinatiering" : "COMBINATION_RING",
    "oorringen" : "CREOLE_EARRINGS",
    "manchetknopen" : "CUFFLINK",
    "oorhangers" : "HOOP_EARRINGS",
    "collier/ketting" : "NECKLACE",
    "hanger/bedel/charm" : "PENDANT",
    "ring" : "RING",
    "ring met edelsteen" : "RING_WITH_GEM",
    "ring met parel" : "RING_WITH_PEARL",
    "set" : "SET",
    "bangle armband" : "SLAVE_BRACELET",
    "oorstekers" : "STUD_EARRINGS",
    "tennisarmband" : "TENNIS_BRACELET",
    "tennis ketting" : "TENNIS_NECKLACE"
}

const Color = {
    "beige":"BEIGE",
    "zwart":"BLACK",
    "blauw":"BLUE",
    "brons":"BRASS",
    "bruin":"BROWN",
    "creme":"CREME",
    "goudkleurig":"GOLD_COLORED",
    "goud zilver kleurig":"GOLD_SILVER_COLORED",
    "groen":"GREEN",
    "grijs":"GREY",
    "parelmoer":"MOTHER_OF_PEARL",
    "parelmoer gekleurd":"MOTHER_OF_PEARL_COLOURED",
    "oranje":"ORANGE",
    "roze":"PINK",
    "paars":"PURPLE",
    "rood":"RED",
    "roségoudkleurig":"ROSE_GOLD_COLORED",
    "rosé zilver gekleurd":"ROSE_SILVER_COLORED",
    "zilverkleurig":"SILVER_COLORED",
    "taupe":"TAUPE",
    "wit":"WHITE",
    "geel":"YELLOW"
}

const jewelMaterial = {
    "goud" : "GOLD",
    "zilver" : "SILVER",
    "hout" : "WOOD",
    "canvas / nylon" : "CANVAS",
    "lederen bovenkant" : "UPPER_LEATHER",
    "pu leder" : "PU_LEATHER",
    "metaal" : "METAL",
    "aluminium" : "ALUMINIUM",
    "keramiek" : "CERAMICS",
    "edelstaal" : "STAINLESS_STEEL",
    "echt leer" : "REAL_LEATHER",
    "titanium": "TITANIUM"
}

const strapMaterial = {
    "aluminium" : "ALUMINIUM",
    "echt leer" : "REAL_LEATHER",
    "lederen bovenkant" : "UPPER_LEATHER",
    "metaal" : "METAL",
    "kunststof / rubber" : "PLASTIC_SILICONE",
    "edelstaal" : "STAINLESS_STEEL",
    "titanium" : "TITANIUM",
    "keramiek" : "CERAMICS",
    "pu leder" : "PU_LEATHER",
    "canvas / nylon" : "CANVAS",
    "echt goud" : "REAL_GOLD"
}

const strapModel = {
    "rekband" : "EXPANDABLE_STRETCH_STRAP",
    "schakelketting zakhorloge" : "SELECT_BAND_FOR_POCKET_WATCH",
    "schakelband" : "SELECT_BAND",
    "milanees / mesh" : "MILANESE_MESH",
    "nato" : "NATO",
    "standaard model" : "STANDARD_MODEL"
}

const caseMaterial = {
    "titanium" : "TITANIUM",
    "edelstaal" : "STAINLESS_STEEL",
    "aluminium" : "ALUMINIUM",
    "metaal" : "METAL",
    "kunststof / rubber" : "PLASTIC_SILICONE",
    "hout" : "WOOD",
    "keramiek" : "CERAMICS"
}


const caseGlasstype = {
    "saffier" : "SAPPHIRE",
    "saffier gecoat" : "SAPPHIRE_COATED",
    "kristal" : "CRYSTAL",
    "mineraal" : "MINERAL",
    "kunststof / rubber" : "SYNTHETIC_PLASTIC",
}

const caseShape = {
    "ovaal" : "OVAL",
    "rechthoek" : "RECTANGLE",
    "rond" : "ROUND",
    "andere vorm" : "OTHER_SHAPE",
    "vierkant" : "SQUARE"
}

const dialIndex = {
    "cijfers (arabisch)" : "ARABIC",
    "puntjes" : "DOTS",
    "romeinse cijfers" : "ROMAN",
    "streepjes" : "STRIPES",
    "geen" : "NONE",
    "sierdiamanten (zirkonia / synthetisch)" : "ORNAMENTAL_DIAMONDS",
    "echte diamanten" : "REAL_DIAMONDS"
}

const dialPattern = {
    "puntjes":"DOTS",
    "streepjes":"STRIPES"
}

const watchIndication = {
    "analoog" : "ANALOG",
    "analoog en digitaal" : "ANALOG_DIGITAL",
    "chrono / multi" : "CHRONO_MULTI",
    "digitaal" : "DIGITAL",
}

const watchMovement = {
    "quartz" : "QUARTZ",
    "solar (zonne energie)" : "SOLAR",
    "gps solar" : "GPS_SOLAR",
    "kinetisch" : "KINETIC",
    "automatisch" : "AUTOMATIC",
    "handopwind" : "HAND_WINDING",
}

const watchType = {
    "smartwatch":"SMARTWATCH",
    "polshorloge":"WRIST",
    "verpleegsterhorloge":"NURSE",
    "zakhorloge":"POCKET"
}

const beringSynonyms = [
    {"key":"Color", "val": "zilver gepolijst", "equals": "SILVER_COLORED"},
    {"key":"Color", "val": "rosegoud gepolijst", "equals": "ROSE_GOLD_COLORED"},
    {"key":"Color", "val": "goud gepolijst", "equals": "GOLD_COLORED"},
    {"key":"Color", "val": "grijs gepolijst", "equals": "GREY"},
    {"key":"Color", "val": "zilver geborsteld", "equals": "SILVER_COLORED"},
    {"key":"Color", "val": "rosegoud geborsteld", "equals": "ROSE_GOLD_COLORED"},
    {"key":"Color", "val": "goud geborsteld", "equals": "GOLD_COLORED"},
    {"key":"Color", "val": "zwart geborsteld", "equals": "BLACK"},
    {"key":"Color", "val": "zwart gepolijst", "equals": "BLACK"},
    {"key":"Color", "val": "blauw geborsteld", "equals": "BLUE"},
    {"key":"Color", "val": "zilver gepolijst/geborsteld", "equals": "SILVER_COLORED"},
    {"key":"Color", "val": "rosegoud   gepolijst/geborsteld", "equals": "ROSE_GOLD_COLORED"},
    {"key":"Color", "val": "goud gepolijst/geborsteld", "equals": "GOLD_COLORED"},
    {"key":"Color", "val": "wit gepolijst", "equals": "WHITE"},
    {"key":"Color", "val": "bruin gepolijst", "equals": "BROWN"},
    {"key":"Color", "val": "blauw gepolijst", "equals": "BLUE"},
    {"key":"Color", "val": "zilver", "equals": "SILVER_COLORED"},
    {"key":"Color", "val": "rosegoud", "equals": "ROSE_GOLD_COLORED"},
    {"key":"Color", "val": "groen mat", "equals": "GREEN"},
    {"key":"Color", "val": "groen gepolijst/geborsteld", "equals": "GREEN"},
    {"key":"Color", "val": "zwart mat", "equals": "BLACK"},
    {"key":"Color", "val": "blauw mat", "equals": "BLUE"},
    {"key":"Color", "val": "zilver mat", "equals": "SILVER_COLORED"},
    {"key":"Color", "val": "rood mat", "equals": "RED"},
    {"key":"Color", "val": "geel mat", "equals": "YELLOW"},
    {"key":"Color", "val": "lila mat", "equals": "PURPLE"},
    {"key":"Color", "val": "lila", "equals": "PURPLE"},
    {"key":"Color", "val": "wit mat", "equals": "WHITE"},
    {"key":"Color", "val": "blauw gepolijst/geborsteld", "equals": "BLUE"},
    {"key":"Color", "val": "sparkling rosegoud", "equals": "ROSE_GOLD_COLORED"},
    {"key":"Color", "val": "sparkling zilver", "equals": "SILVER_COLORED"},
    {"key":"Color", "val": "grijs geborsteld", "equals": "GREY"},
    {"key":"Color", "val": "donker grijs", "equals": "GREY"},
    {"key":"Color", "val": "bruin/zwart", "equals": "BROWN"},
    {"key":"Color", "val": "rosegoud gepolijst", "equals": "ROSE_GOLD_COLORED"},
    {"key":"Color", "val": "zwart/goud", "equals": "GOLD_COLORED"},
    {"key":"Color", "val": "zwart gepolijst/geborsteld", "equals": "BLACK"},
    {"key":"Color", "val": "weiß", "equals": "WHITE"},
    {"key":"Color", "val": "rosegoud   mat", "equals": "ROSE_GOLD_COLORED"},
    {"key":"Color", "val": "donker grijs geborsteld", "equals": "GREY"},
    {"key":"Color", "val": "grijs gepolijst/geborsteld", "equals": "GREY"},
    {"key":"Color", "val": "zilver/goud gepolijst/geborsteld", "equals": "GOLD_SILVER_COLORED"},
    {"key":"Color", "val": "zilver/goud gepolijst", "equals": "GOLD_SILVER_COLORED"},
    {"key":"Color", "val": "sparkling bruin", "equals": "BROWN"},
    {"key":"Color", "val": "sparkling rood", "equals": "RED"},
    {"key":"Color", "val": "sparkling zwart", "equals": "BLACK"},
    {"key":"Color", "val": "rosegoud   gepolijst", "equals": "ROSE_GOLD_COLORED"},
    {"key":"Color", "val": "goud", "equals": "GOLD_COLORED"},
    {"key":"Color", "val": "rosegoud   geborsteld", "equals": "ROSE_GOLD_COLORED"},
    {"key":"Color", "val": "veelkleurig", "equals": ""},
    {"key":"Color", "val": "sparkling goud", "equals": "ROSE_GOLD_COLORED"},
    {"key":"Color", "val": "met meedere kleuren", "equals": ""},
    {"key":"Color", "val": "ivoor", "equals": "WHITE"},
    {"key":"Color", "val": "zwart mat", "equals": "BLACK"},
    {"key":"Color", "val": "groen/paars", "equals": "GREEN"},
    {"key":"Color", "val": "ip gun", "equals": "GREY"},
    {"key":"Color", "val": "donker grijs geborsteld", "equals": "GREY"},
    {"key":"Color", "val": "grijs", "equals": "GREY"},

    {"key": "jewelType", "val": "binnenring", "equals": "RING"},
    {"key": "jewelType", "val": "buitenring", "equals": "RING"},
    {"key": "jewelType", "val": "charm", "equals": "CHARM"},
    {"key": "jewelType", "val": "halsketting", "equals": "NECKLACE"},
    {"key": "jewelType", "val": "ketting & charm", "equals": "NECKLACE"},
    {"key": "jewelType", "val": "oorknopje", "equals": "STUD_EARRINGS"},
    {"key": "jewelType", "val": "oorring", "equals": "CREOLE_EARRINGS"},
    {"key": "jewelType", "val": "ring combinatie", "equals": "COMBINATION_RING"},
    {"key": "jewelType", "val": "armband combinatie", "equals": "BRACELET"},

    {"key": "caseMaterial", "val": "roestvrij staal", "equals": "STAINLESS_STEEL"},
    {"key": "caseMaterial", "val": "titaan", "equals": "TITANIUM"},

    {"key": "strapMaterial", "val": "kalfsleer", "equals": "REAL_LEATHER"},
    {"key": "strapMaterial", "val": "milanees", "equals": "STAINLESS_STEEL"},
    {"key": "strapMaterial", "val": "nato", "equals": "CANVAS"},
    {"key": "strapMaterial", "val": "roestvrij staal bandje met keramiek elementjes", "equals": "STAINLESS_STEEL"},
    {"key": "strapMaterial", "val": "roestvrij staal bandje met roestvrij staal elementjes", "equals": "STAINLESS_STEEL"},
    {"key": "strapMaterial", "val": "satijnleer", "equals": "REAL_LEATHER"},
    {"key": "strapMaterial", "val": "silicon", "equals": "PLASTIC_SILICONE"},
    {"key": "strapMaterial", "val": "titaan bandje met titaan elementjes", "equals": "TITANIUM"},

    {"key": "jewelMaterial", "val": "roestvrij staal, keramiek", "equals": "CERAMICS"},
    {"key": "jewelMaterial", "val": "kalfsleer", "equals": "REAL_LEATHER"},
    {"key": "jewelMaterial", "val": "mesh", "equals": "STAINLESS_STEEL"},
    {"key": "jewelMaterial", "val": "roestvrij staal", "equals": "STAINLESS_STEEL"},
    {"key": "jewelMaterial", "val": "roestvrij staal, parelmoer", "equals": "STAINLESS_STEEL"},
    {"key": "jewelMaterial", "val": "sodaliet/roestvrij staal", "equals": "STAINLESS_STEEL"},
    {"key": "jewelMaterial", "val": "925 zilver", "equals": "SILVER"},
    {"key": "jewelMaterial", "val": "rode tijgeroog/keramiek/roestvrij staal", "equals": "STAINLESS_STEEL"},
    {"key": "jewelMaterial", "val": "Rode Tijgeroog/Keramiek/Roestvrij Staal", "equals": "STAINLESS_STEEL"},

    {"key": "strapModel", "val": "milanees", "equals": "MILANESE_MESH"},

    {"key": "caseGlasstype", "val": "kunststof", "equals": "SYNTHETIC_PLASTIC"},
    {"key": "caseGlasstype", "val": "saffierglas plat", "equals": "SAPPHIRE"},
    {"key": "caseGlasstype", "val": "saffierglas gebogen", "equals": "SAPPHIRE"},

    {"key": "watchMovement", "val": "automaat", "equals": "AUTOMATIC"},
    {"key": "watchMovement", "val": "kwarts", "equals": "QUARTZ"},
    {"key": "watchMovement", "val": "radio", "equals": "QUARTZ"},
    {"key": "watchMovement", "val": "solar", "equals": "SOLAR"}
]



function init(productData, attributeData) {

    productData = parseJson(productData)
    attributeData = parseJson(attributeData)

    var attributesToBeUsed = attributeData.rss.channel.item.map(function(p) {
        return {
            id:p['g:id'],
            collectie:p["g:propid_26"],
            categorie:p["g:propid_52"],
            gender:p["g:propid_48"],
            horlogeglas:p["g:propid_28"],
            uurwerk:p["g:propid_43"],
            kastkleur:p["g:propid_38"],
            kastdikte:p["g:propid_51"],
            kastbreedte:p["g:propid_37"],
            kastdiameter:p["g:propid_54"],
            kastmateriaal:p["g:propid_29"],
            bandmateriaal:p["g:propid_42"],
            bandkleur:p["g:propid_35"],
            bandbreedte:p["g:propid_49"],
            sluiting:p["g:propid_50"],
            wijzerplaatkleur:p["g:propid_34"],
            waterdichtheid:p["g:propid_40"],
            sieraadmateriaal:p["g:propid_44"],
            materiaal_link:p["g:propid_39"],
            sieraadkleur:p["g:propid_46"],
            kleur_link:p["g:propid_41"],
            sieraaddiameter:p["g:propid_56"],
            sieraadbreedte:p["g:propid_55"],
            kleur_bezel:p["g:propid_31"],
            coating:p["g:propid_47"],
            type:p["g:propid_53"]
        }
    });
    
    const res = beringParser(productData,attributesToBeUsed)

    let now = String(Math.floor(Date.now() / 1000))

   //Save the results to a JSON-file
    if(res && res.data && res.data.length > 0 && process.argv[2] === "--save") {
        
        fs.writeFile("./output/"+now+".json", JSON.stringify(res.data,null,2), function(err) {if(err) { throw new Error(err); }});
        console.log('result saved to /output')

        if(res.problems) {
            fs.writeFile("./output/"+now+"_problems.json", JSON.stringify(res.problems,null,2), function(err) {if(err) { throw new Error(err); }});
            console.log('problems saved to /output')
        }

        if(res.warnings) {
            fs.writeFile("./output/"+now+"_warnings.json", JSON.stringify(res.warnings,null,2), function(err) {if(err) { throw new Error(err); }});
            console.log('warnings saved to /output')

        }

        if(res.notifications) {
            fs.writeFile("./output/"+now+"_notifications.json", JSON.stringify(res.notifications,null,2), function(err) {if(err) { throw new Error(err); }});
            console.log('notifications saved to /output')

        }
    }
}



async function getJson() {
    const data1 = await fs.readFile('./src/bering-api-output.json','utf8')
    const data2 = await fs.readFile('./src/bering-api-output-all-information.json','utf8')

    init(data1,data2)
}

getJson()

// fs.readFile('./src/bering-api-output.json','utf8', function(errors,data) {

//     if(data) {
//         var jsonData = parseJson(data)
//         if(jsonData) {

//             // console.log(jsonData)
//             init(jsonData);

//         } else {
//             throw new Error("Invalid JSON input")
//         }
//     } else {
//         throw new Error("No JSON input")
//     }
// });