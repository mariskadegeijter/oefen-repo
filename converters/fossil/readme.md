1. Create the directories `/src` and `/output` here
2. Place a CSV file in /src. The name should contain the word 'input'
3. install the dependencies: `npm install`
4. run `node fossil.js`
