const fs=require('fs')
const c2j=require('csvtojson')
const j2c = require('json-2-csv');


function currentDir() {
    return new Promise(function(resolve, reject) {
        fs.readdir('./src','utf-8', function(err, filenames){
            if (err) {
                reject(err); 
            } else {

                let filesInDir = []

                filenames.forEach((fname) => {
                    if(String(fname).includes('input') && String(fname).endsWith('.csv')) {
                        filesInDir.push(fname)
                    }
                })

                resolve(filesInDir);
            }
        });
    });
};

currentDir().then((files) => {

    parseCsv(files[0])

}).catch((error) => console.log(error));

function parseCsv(inputFile) {

    console.log('Reading '+inputFile+'..')

    c2j({delimiter:";"}).fromFile('./src/'+inputFile).then((articles)=>{

        let results = []

        articles.forEach((art) => {
            
            result = {
                'type':'jewel',
                'Artcode':'',
                'Merk':'',
                'Adviesprijs':'',
                'EAN':'',
                'Categorie':'JEWEL',
                'Korte omschrijving NL':'',
                'Productnaam NL':'',
                'foto1':'',
                'foto2':'',
                'foto3':'',
            }

            result['Geslacht'] = art['gender']

            result['Artcode'] = art['sku']
            result['Merk'] = art['brand']
            result['Sieraadtype'] = art['jewelry sub type']
            result['Productnaam NL'] = art['product name']
            result['Korte omschrijving NL'] = art['product description']

            result['jMateriaal'] = art['primary material jewelry']
            result['jKleur'] = art['primary color jewelry']
            result['jHoogte'] = ""
            result['jBreedte'] = art['jewelry width']
            result['jDikte'] = ""
            result['jDiameter'] = ""
            result['jMaat'] = ""

            results.push(result)

            // sku: 'SKJ1553710',
            // 'available sizes jewelry': '',
            // brand: 'Skagen',
            // 'chain length': '',
            // 'closure type jewelry': 'Niet van toepassing',
            // gender: 'Dames',
            // 'jewelry length': '7,2?mm',
            // 'jewelry sub type': 'Middelpunt',
            // 'jewelry width': '5.1MM',
            // keywords: 'skagen, denemarken, sieraden, damessieraden, sieraden voor dames, damesring, ring voor dames, zeeglas',
            // 'measurements jewelry': '7,2?mm x 5,1?mm x 9,8?mm',
            // platform: 'SEA GLASS',
            // 'precious metal weight': '0',
            // 'primary color jewelry': 'Goud',
            // 'primary finish jewelry': 'Gepolijst',
            // 'primary material jewelry': 'Roestvrij staal',
            // 'product description': 'Deze middenfocale ring is gemaakt van goudkleurig roestvrij staal en geaccentueerd met glas.',
            // 'product name': 'Sea Glass Gold-Tone Stainless Steel Center Focal Ring',
            // 'product type': 'Sieraden',
            // 'secondary color jewelry': 'Wit',
            // 'secondary finish jewelry': 'Glossy',
            // 'secondary material jewelry': 'Glas',
            // 'silhouette jewelry': 'Ring',
            // 'stone color jewelry': 'NA',
            // 'stone count': '0',
            // 'Stone Type': '',
            // 'stone weight': '',
            // 'total product gross weight g': ''

            // console.log(art)

        }); 

        j2c.json2csvAsync(results,{delimiter:{field:";"}}).then(csv => {
            fs.writeFileSync('./output/results.csv', csv);
            console.log('Saved to output/results.csv')
        }).catch(err => console.log(err));
    })
}
