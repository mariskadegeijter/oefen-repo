# Seiko API Data Parser

This parser converts Seiko API data to a valid PrismaNote product JSON object

Dry run: `node seiko.js`  
Save output: `node seiko.js --save`



## Todo 8-12

- [x] `watch.strap.strapWristPermimeter` ->  `watch.strap.wristPermimeter`
- [x] Move productNumber and Ean from root to `variants[0]`
- [x] Assign productNumber also to to `insertedProductNumber`
- [ ] Assign starting number to `brandStratingDigits`
- [x] `hasStock` alsways true
- [x] `watch.hasDateFunction` fill this based on DATUM/TIJD property
- [x] `watch.isSmartWatch`: //true if type != quartz/solar/automaat/handopwind
- [x] `nl.name` : add productnumber to title
- [x] `en.name` : create simple translation: "{brand} watch"
- [x] if `LICHTGEVENDEINDEX:'Ja' or LICHTGEVENDEWIJZERS:'Ja'`, then `hasLightFunction:true`
- [x] if `MATERIAALLUNETTE:'Staal/zilverkleurig'`: set `case.designItem["BEZEL"]`
- [x] `MATERIAALLUNETTE` : split at `/`, if color, then set `case.color` to this color (if only `Staal` then `SILVER`)
- [x] if `SCHROEFDEKSEL:'Ja'`: set `case.designItem["SCREW"]`
- [x] `watch.watch.waterProofLevel`: if "waterresistant", then set to `0` (zero, not `null`)

### watch.type:
```
if(type != digital) {
	if 2 or more of these is true:
		24-UURWIJZER':'Ja',
		YACHTINGTIMER:'Nee',
		GMT:'Nee',
		TACHYMETER:'Nee',
		TWEEDETIJDZONE:'Nee',
		WORLDTIMER:'Nee',
		--> set type to CHRONO_MULTI
	else
		type is analog
}

```