'use strict';
const fs = require('fs');

var messages = []

function _removeAccents(strAccents) {
    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents =    "ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž";
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
        } else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');
    return strAccentsOut;
}

function _chkVal(val) {
	if(Array.isArray(val)) {
		return Boolean(val.length > 0 && val[0] !== "")
	} else {
		return Boolean(val)
	}
}

function _parseATMLevel(val, productNumber) {
	let res = null
	if(isNaN(val)) {
		let numeric = _getNumberFromString(val.replace(" ",""))
		if(_chkVal(numeric) && !isNaN(numeric)) {
			if(numeric => 30 && numeric <= 1000) {
				res = numeric/10
			}
		} else {
			if(val.toLowerCase().trim() === 'waterresistant') {
				res = 0
			}
		}
	}

	if(res === null) {
		messages.push({"product":productNumber,"problem":String("Cannot parse ATM '"+val+"'")})
	}

	return res
}

function _parseHasDateFunction(val, productNumber) {

	let dateFunction = String(val.toLowerCase().trim())

	if(["datum","geen datum", "dag/datum"].includes(dateFunction)) {
		return dateFunction.includes('geen') ? false : true
	} else {
		messages.push({"product":productNumber,"problem":String("Invalid DATUMTIJD value: '"+val+"'")})
		return false
	}

}

function _isSmartWatch(val, handWinding) {

	let type = String(val.toLowerCase().trim())

	if (type !== "" && /quartz|auto|solar|hand/.test(type) && handWinding === false) {
		return false
	} else {
		return true
	}
}

function _parseYesNo(val) {
	return val && String(val).toLowerCase() === "ja" ? true : false
}

function _getNumberFromString(str) {

	return ((/\d+/).test(str) ? str.match(/\d+/)[0] : false)
}

function _convertSizeStringToMm(str,attr=null) {

	str = String(str).replace(",",".").trim()

	if(str.includes("cm")) {
		str = str.replace("cm","").replace(" ","").trim()
		str = parseFloat(str)*10
	} else if(str.includes("mm")) {
		str = parseFloat(str.replace("mm","").replace(" ","").trim())
	} else {
		str = ""
	}
	
	if(typeof str === 'number') {
		return str
	} else {
		return null
	}
}

function parseJson(data) {
	try {
	    var res = JSON.parse(data);
	} catch(err) {
	    var res = null
	}
	return res
}


function buildShortDescription(article,properties,productNumber) {

	let params = {
		"gender":"",
		"brand":"",
		"type":"",
		"material":"",
		"caseSize":"",
		"caseDepth":"",
		"dialColor":"",
		"caseShape":"",
		"caseColor":"",
		"strapClasp":"",
		"strapWidth":"",
		"wristPermimeter":"",
		"additionalSpecs":[],
	}

	let result = ""

	//check if any of the required values is missing
	let hasMissingValues = [
		article.brand.name,properties.MATERIAALKAST,
		properties.SOORTHORLOGE,
		article.watch.case.size,
		//article.watch.case.depth
	].filter(function(p) {
		return typeof p === 'undefined' || !p || p === '' || p === 0;
	})

	if(hasMissingValues.length === 0) {
	
		if(article.kids) {
			if(article.male && article.female) {
				params.gender = "kinder"
			} else {
				if(article.male) {
					params.gender = "jongens"
				}
				if(article.female) {
					params.gender = "meisjes"
				}
			}
		} else {
			if(article.male) {
				params.gender = "heren"
			}
			if(article.female) {
				params.gender = "dames"
			}
		}

		params.brand = _chkVal(article.brand.name) ? article.brand.name : ""
		params.type = _chkVal(properties.SOORTHORLOGE) ? properties.SOORTHORLOGE.toLowerCase() : ""
		params.material = _chkVal(properties.MATERIAALKAST) ? properties.MATERIAALKAST.toLowerCase() : ""
		params.caseSize = _chkVal(article.watch.case.size) ? String(article.watch.case.size + " mm") : ""
		params.caseDepth = _chkVal(article.watch.case.depth) ? String(article.watch.case.depth + " mm") : ""

		//Additional specs
		if(_chkVal(properties.KASTVORM)) {
			params.additionalSpecs.push(String("Kastvorm: " + properties.KASTVORM.toLowerCase()))
		}

		if(_chkVal(properties.KLEURKAST)) {
			params.additionalSpecs.push(String("Kastkleur: " + properties.KLEURKAST.toLowerCase()))
		}

		if(_chkVal(properties.MATERIAALKAST)) {
			params.additionalSpecs.push(String("Kastmateriaal: "+properties.MATERIAALKAST.toLowerCase()))
		}

		if(_chkVal(article.watch.strap.width)) {
			params.additionalSpecs.push(String("Bandbreedte: "+ article.watch.strap.width + " mm"))
		}

		if(_chkVal(article.watch.strap.wristPermimeter)) {
			params.additionalSpecs.push(String("Bandlengte: "+ article.watch.strap.wristPermimeter + " mm"))
		}

		if(_chkVal(properties.TYPESLUITING)) {
			params.additionalSpecs.push(String("Sluiting: "+ properties.TYPESLUITING.toLowerCase()))
		}

		if(_chkVal(properties.KLEURWIJZERPLAAT)) {
			params.additionalSpecs.push(String("Wijzerplaatkleur: "+ properties.KLEURWIJZERPLAAT.toLowerCase()))
		}

		if(_chkVal(properties.SOORTGLAS)) {
			params.additionalSpecs.push(String("Glassoort: "+properties.SOORTGLAS.toLowerCase()))
		}

		if(_chkVal(article.watch.waterProofLevel)) {
			params.additionalSpecs.push(String("Waterdichtheid: "+article.watch.waterProofLevel+" ATM"))
		}

		if(_chkVal(properties.TECHNIEK)) {
			params.additionalSpecs.push(String("Techniek: "+properties.TECHNIEK.toLowerCase()))
		}

		result = String("Dit is een "+params['gender']+"horloge van het merk "+params['brand']+". ");

		result += "Het is een "+params['type']+" horloge gemaakt van "+params['material']+". "
		result += "De diameter van de kast is "+params['caseSize']+" mm"
		
		if(params['caseDepth']) {
			result += " en de kast is "+params['caseDepth']+" mm dik"
		}
		
		result += "."

		if(params.additionalSpecs.length > 0) {
			result += "\nDe verdere specificaties van het horloge zijn als volgt:\n"
			result += params.additionalSpecs.join("\n")
		}

	} else {
		messages.push({"product":productNumber, "problem":"Could not generate short description, value missing"})
	}

	return result
}

//assigns the right values from the properties to the final article
function parseProperties(article,properties,currentProductNumber) {

	if(_chkVal(properties.GEWICHTHORLOGE)) { article.weight = parseFloat(properties.GEWICHTHORLOGE) }
	if(_chkVal(properties.BANDBREEDTE)) { article.watch.strap.width = _convertSizeStringToMm(properties.BANDBREEDTE,"BANDBREEDTE") }
	if(_chkVal(properties.BANDLENGTE)) { article.watch.strap.wristPermimeter = _convertSizeStringToMm(properties.BANDLENGTE,"BANDLENGTE") }
	if(_chkVal(properties.DIKTEVANKAST)) { article.watch.case.depth = _convertSizeStringToMm(properties.DIKTEVANKAST,"DIKTEVANKAST") }
	if(_chkVal(properties.KASTVORM)) { article.watch.case.shape = getPrismaNoteProperty('caseShape',properties.KASTVORM,"KASTVORM",currentProductNumber) }
	
	if(_chkVal(properties.DIAMETERKAST)) {

		let watchSize = _convertSizeStringToMm(properties.DIAMETERKAST,"DIAMETERKAST")

		article.size = watchSize
		article.variants[0].size = watchSize

		article.watch.case.size = _convertSizeStringToMm(properties.DIAMETERKAST,"DIAMETERKAST")
	}

	if(_chkVal(properties.KLEURBAND)) {
		article.watch.strap.color = getPrismaNoteProperty('Color',properties.KLEURBAND,"KLEURBAND",currentProductNumber)	
	}

	if(_chkVal(properties.KLEURKAST)) {
		article.watch.case.color = getPrismaNoteProperty('Color',properties.KLEURKAST,"KLEURKAST",currentProductNumber)	
	}

	if(_chkVal(properties.KLEURWIJZERPLAAT)) {
		article.watch.dial.color = getPrismaNoteProperty('Color',properties.KLEURWIJZERPLAAT,"KLEURWIJZERPLAAT",currentProductNumber)	
	}

	if(_chkVal(properties.LICHTFUNCTIE)) {
		article.watch.hasLightFunction = _parseYesNo(properties.LICHTFUNCTIE)
	}

	//if light function has been set to false, try to set it by checking the properties LICHTGEVENDEINDEX and LICHTGEVENDEWIJZERS
	if(!article.watch.hasLightFunction) {
		if(_chkVal(properties.LICHTGEVENDEINDEX)) {
			article.watch.hasLightFunction = _parseYesNo(properties.LICHTGEVENDEINDEX)
		}
	}
	if(!article.watch.hasLightFunction) {
		if(_chkVal(properties.LICHTGEVENDEWIJZERS)) {
			article.watch.hasLightFunction = _parseYesNo(properties.LICHTGEVENDEWIJZERS)
		}
	}

	if(_chkVal(properties.MATERIAALBAND)) {
		article.watch.strap.material = getPrismaNoteProperty('strapMaterial',properties.MATERIAALBAND,"MATERIAALBAND",currentProductNumber)	
	}

	if(_chkVal(properties.MATERIAALKAST)) {
		article.watch.case.material = getPrismaNoteProperty('caseMaterial',properties.MATERIAALKAST,"MATERIAALKAST",currentProductNumber)	
	}

	if(_chkVal(properties.SOORTGLAS)) {
		article.watch.case.glassMaterial = getPrismaNoteProperty('caseGlasstype',properties.SOORTGLAS,"SOORTGLAS",currentProductNumber)	
	}

	if(_chkVal(properties.SOORTHORLOGE)) {

		article.watch.indication = getPrismaNoteProperty('watchIndication',properties.SOORTHORLOGE,"SOORTHORLOGE",currentProductNumber)	

		let watchType = properties.SOORTHORLOGE.toLowerCase().trim()

		if(watchType && watchType !== 'digitaal') {
			let propTrueFilter  = [
				properties['24UURWIJZER'],
				properties.YACHTINGTIMER,
				properties.GMT,
				properties.TACHYMETER,
				properties.TWEEDETIJDZONE,
				properties.WORLDTIMER
			].filter(function(p) {
				return p && p.toLowerCase() !== 'nee'
			});

			let oldVal = article.watch.indication

			//if two or more attributes in propTrueFilter are true, we can safely guess it's a chrono multi watch
			if(oldVal !== 'CHRONO_MULTI') {
				if(propTrueFilter && propTrueFilter.length > 1) {
					article.watch.indication = "CHRONO_MULTI"
					messages.push({"product":currentProductNumber,"notification":"indication changed from "+oldVal+" to CHRONO_MULTI based on other attributes"})
				} else {
					if(article.watch.indication !== "ANALOG") {
						article.watch.indication = "ANALOG"
						messages.push({"product":currentProductNumber,"notification":"indication changed from "+oldVal+" to ANALOG based on other attributes"})
					}
				}
			}
		}
	}

	if(_chkVal(properties.TECHNIEK)) {
		article.watch.movement = getPrismaNoteProperty('watchMovement',properties.TECHNIEK,"TECHNIEK",currentProductNumber)
		article.watch.isSmartWatch = _isSmartWatch(properties.TECHNIEK, _parseYesNo(properties.HANDOPWINDINGMOGELIJK))
	}

	if(_chkVal(properties.WATERDICHTHEID)) {
		let res = _parseATMLevel(properties.WATERDICHTHEID, currentProductNumber)
		if(res) { 
			article.watch.waterProofLevel = res
		}
	}

	if(_chkVal(properties.DATUMTIJD)) {
		article.watch.hasDateFunction = _parseHasDateFunction(properties.DATUMTIJD, currentProductNumber)
	}

	if(_chkVal(properties.MATERIAALLUNETTE)) {

		if(!_chkVal(article.watch.case.designItems)) {
			article.watch.case.designItems = []
		}

		article.watch.case.designItems.push("BEZEL")
	}

	if(_chkVal(properties.SCHROEFDEKSEL) && _parseYesNo(properties.SCHROEFDEKSEL)) {
		if(!_chkVal(article.watch.case.designItems)) {
			article.watch.case.designItems = []
		}
		article.watch.case.designItems.push("SCREW")
	}
	
	if(_chkVal(properties.SEXE)) {
		switch(properties.SEXE) {
			case "Heren":
				article.male = true
			break;

			case "Dames":
				article.female = true
			break;

			case "Kinder":
				article.kids = true
			break;
		}
	} else {
		messages.push({
			"product":currentProductNumber,
			"problem":"cant define gender"
		})
	}

	return article
}

fs.readFile('./src/seiko-api-input.json','utf8', function(errors,data) {
	if(data) {
		var jsonData = parseJson(data)
		if(jsonData) {
			init(jsonData);
		} else {
			throw new Error("Invalid JSON input")
		}
	} else {
		throw new Error("No JSON input")
	}
});

function seikoParser(srcData) {

	let productsFound = []

	if(srcData.Articles) {

		let articles = srcData.Articles
		let currentBrandName = ""
		let currentProductNumber = ""

		if (articles.Brand && articles.Brand.length > 0) {
		    
			articles.Brand.forEach(function(brand) {
		    	currentBrandName = brand.$.name
		    	if(brand.Article && brand.Article.length > 0) {
					//we have articles in the brand
		        	brand.Article.forEach(function(art) {
		                
						//article object created from blueprint
		          		let article = {
							"productNumber":"",
							"eanNumber":"",
							"images":[],
							"variants": [],
							"hasStock":true,
							"size":0,
							"weight":0,
							"totalCaratWeight":0,
							"strap":[],
							"watch":{
								"type":"WRIST",
								"movement":"",
								"indication":"",
								"hasSwissMovement":false,
								"hasDateFunction":false,
								"waterProofLevel":null,
								"isNickelFree":false,
								"isAntiAllergy":false,
								"hasLightFunction":false,
								"isSmartWatch":false,
								"smartWatchFunctions":"",
								"dial":{
									"color":"",
									"pattern":"",
									"print":"",
									"index":""
								},
								"case":{
									"shape":"",
									"size":0,
									"depth":0,
									"material":"",
									"glassMaterial":"",
									"color":""
								},
								"strap":{
									"model":"",
									"width":0.0,
									"material":"",
									"color":"",
									"print":"",
									"studs":"",
									"clasp":"",
									"pattern":"",
									"wristPermimeter":""
								}
							},
							"category":"",
							"brand":{
								"_id":"",
								"name":"",
								"alias":"",
								"nameSlug":"",
								"description":"",
								"images":[],
								"startingDigits":"",
								"isPoolArticleBrand":false
							},
							"suggestedRetailPrice":0,
							"suggestedRetailPriceVat":0,
							"male":false,
							"female":false,
							"kids":false,
							"nl":{
								"name":"",
								"shortDescription":"",
								"longDescription":""
							},
							"en":[]
		          		}

						//set productnumber
						currentProductNumber = _chkVal(art.ArticleId) ? String(art.ArticleId[0]) : ""

						//brandname
		          		article.brand.name = currentBrandName
		          		article.category = "WATCH"

						if(_chkVal(art.ImageUri) && _chkVal(art.Description)) {
							article.images.push({
								src:art.ImageUri[0],
								alt: (_chkVal(art.Description[0]) ? art.Description[0] : currentProductNumber)
							})
						} else {
							if(!_chkVal(art.ImageUri)) { messages.push({"product":art.ArticleId[0],"problem":"has no image url"}) }
							if(!_chkVal(art.Description)) { messages.push({"product":art.ArticleId[0],"problem":"has no description as for image alt"}) }
						}

						let variant = {
							"productNumberAuto":false,
							"size":0,
							"productNumber":currentProductNumber,
							"insertedProductNumber":currentProductNumber,
							"ean":_chkVal(art.Barcode) ? String(art.Barcode[0]) : "",
							"shopStratingDigits":"" //no starting digits for the Seiko Brands
						}
					
						article.variants.push(variant)

						var properties = {}

						//collect all xml attributes from the data, and add them to properties
		                if(art.Properties && art.Properties.length > 0) {
		                    if(art.Properties[0].PropertyGroup) {
		                        art.Properties[0].PropertyGroup.forEach(function(propGroup){
		                            if(propGroup.$.name) {
		                                propGroup.Property.forEach(function(prop){
		                                	let propName = String(prop.$.name).split(' ').join('').replace('/','').replace('-','').toUpperCase()

		                                	if(propName === "GEWICHTHORLOGE") {
		                                		if(_getNumberFromString(prop._)) {
		                                			prop._ = _getNumberFromString(prop._)
		                                		} else {
		                                			messages.push({"product":art.ArticleId[0],"problem":"could not extract weight from properties"})
		                                		}
		                                	}
											
		                                    properties[propName] = prop._
		                                });
		                            }
		                        });
		                    }
		                }
		             
		             	article = parseProperties(article,properties,currentProductNumber)

						if(_chkVal(art.Description)) {
							article.nl.name = String(art.Description[0] + " " + art.ArticleId[0])
							article.nl.name = String(currentBrandName + " Watch " + art.ArticleId[0])
						}
		             	article.nl.shortDescription = buildShortDescription(article,properties,currentProductNumber)

		                productsFound.push(article)
		          });
		      	}
		    });
		}
	} else {
		throw new Error("No articles in source data")
	}

	console.log("-- Finished --")
	console.log("Products: "+productsFound.length)

	let problems = messages.filter(m => { return m.problem && m.problem !== '' });
	let warnings = messages.filter(m => { return m.warning && m.warning !== '' });
	let notifications = messages.filter(m => { return m.notification && m.notification !== '' });

	console.log("Problems: ",problems.length)
	console.log("Warnings: ",warnings.length)
	console.log("Notifications: ",notifications.length)

	return {
		"data":productsFound,
		"problems":problems,
		"warnings":warnings,
		"notifications":notifications
	};
}

function getPrismaNoteProperty(type,val,srcProp,productNumber=null) {

	val = _removeAccents(val).toLowerCase()

	if(!["caseGlasstype","caseMaterial","caseShape","Color","dialIndex","dialPattern","gemKind ","jewelMaterial","jewelType","strapMaterial","strapModel","watchIndication","watchMovement","watchType"].includes(type)) {
		return null
	}

	let res = {
		"success":false,
		"data":"",
		"errors":[]
	}

	let targetProp = null

	switch(type) {

		case "caseGlasstype":
			targetProp = caseGlasstype
		break;
		case "caseMaterial":
			targetProp = caseMaterial	
		break;
		case "caseShape":
			targetProp = caseShape		
		break;
		case "Color":
			targetProp = Color
		break;
		case "dialIndex":
			targetProp = dialIndex
		break;
		case "dialPattern":
			targetProp = dialPattern
		break;
		case "gemKind ":
			targetProp = gemKind	
		break;
		case "jewelMaterial":
			targetProp = jewelMaterial	
		break;
		case "jewelType":
			targetProp = jewelType
		break;
		case "strapMaterial":
			targetProp = strapMaterial
		break;
		case "strapModel":
			targetProp = strapModel
		break;
		case "watchIndication":
			targetProp = watchIndication
		break;
		case "watchMovement":
			targetProp = watchMovement
		break;
		case "watchType":
			targetProp = watchType
		break;

		default:
			targetProp = null
		break;
	}

	if(_chkVal(targetProp)) {
		if(val in targetProp) { //there's an exact match
			res.success = true
			res.data = targetProp[val]
		} else { //there's no match, so let's try one of the synonyms
			seikoSynonyms.forEach(s => {
				if(type === s['key'] && val === s['val']) {
					res.success = true
					res.data = s['equals']
					messages.push({"product":(productNumber ? productNumber : "(no productnumber)"),"notification":String(type+" '"+val+"' - alias found: "+s['equals'])})
				}
			});

			if(!res.success) {
				seikoArticleOverrides.forEach(s => {
					if(s['attr'] === type && s['of'] === srcProp && s['sku'] === productNumber) {
						res.data = s['equals']
						res.success = true
						messages.push({"product":(productNumber ? productNumber : "(no productnumber)"),"notification":String(type+" '"+val+"' overwritten by "+s['equals'])})
					}
				});

				if(!res.success) {
					console.log("Not found: ",productNumber,srcProp,type)
				}
			}
		}
	}

	if(res.success) {
		return res.data
	} else {
		messages.push({"product":(productNumber ? productNumber : "(no productnumber)"),"problem":String(type+" '"+val+"' not found for "+(srcProp ? srcProp : ""))})
		return;
	}
}

function init(productData) {
	
	const res = seikoParser(productData)

	let now = String(Math.floor(Date.now() / 1000))

   //Save the results to a JSON-file
	if(res && res.data && res.data.length > 0 && process.argv[2] === "--save") {
		
		fs.writeFile("./output/"+now+".json", JSON.stringify(res.data,null,2), function(err) {if(err) { throw new Error(err); }});
		console.log('result saved to /output')

		if(res.problems) {
			fs.writeFile("./output/"+now+"_problems.json", JSON.stringify(res.problems,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('problems saved to /output')
		}

		if(res.warnings) {
			fs.writeFile("./output/"+now+"_warnings.json", JSON.stringify(res.warnings,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('warnings saved to /output')

		}

		if(res.notifications) {
			fs.writeFile("./output/"+now+"_notifications.json", JSON.stringify(res.notifications,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('notifications saved to /output')

		}
	}
}

const gemKind = {
	"agaat" : "AGATE",
	"amazone" : "AMAZONE",
	"amazoniet" : "AMAZONITE",
	"barnsteen" : "AMBER",
	"amethyst" : "AMETHYST",
	"aquamarijn" : "AQUAMARINE",
	"aventurijn" : "AVENTURINE",
	"beryl" : "BERYLL",
	"bloedkoraal" : "BLOODCORAL",
	"calciet" : "CALCITE",
	"cameo" : "CAMEO",
	"carneool" : "CARNELEAN",
	"cateye" : "CATEYE",
	"keramiek" : "CERAMICS",
	"chalcedoon" : "CHALCEDONY",
	"citrien" : "CITRINE",
	"korund" : "CORUNDUM",
	"kristal" : "CRYSTAL",
	"diamant" : "DIAMOND",
	"smaragd" : "EMERALD",
	"glazuur" : "ENAMEL",
	"epidote" : "EPIDOTE",
	"firepost" : "FIREPOST",
	"fluoriet" : "FLUORITE",
	"granaat" : "GARNET",
	"glas" : "GLASS",
	"heliotroop" : "HELIOTROPE",
	"hematiet" : "HEMATITE",
	"iriskwarts" : "IRISQUARTZ",
	"jade" : "JADE",
	"jargon" : "JARGON",
	"jasper" : "JASPER",
	"labradoriet" : "LABRADORITE",
	"lapis" : "LAPIS",
	"lazuli" : "LAZULI",
	"lazuriet" : "LAZURITE",
	"citroenkwarts" : "LEMONQUARTZ",
	"madeiracitrine" : "MADEIRACITRINE",
	"malachiet" : "MALACHITE",
	"maansteen" : "MOONSTONE",
	"muranoglass" : "MURANOGLASS",
	"natuursteen" : "NATURALSTONE",
	"nefriet" : "NEPHRITE",
	"onyx" : "ONYX",
	"opaal" : "OPAL",
	"parel" : "PEARL",
	"parel akoya" : "PEARLAKOYA",
	"parel cultive" : "PEARLCULTIVE",
	"parel zuidzee" : "PEARLSOUTHSEA",
	"parel zoetwater" : "PEARLSWEETWATER",
	"parel synthetisch" : "PEARLSYNTHETICAL",
	"parel tahiti" : "PEARLTAHITI",
	"peridoot" : "PERIDOTE",
	"fosforiet" : "PHOSPHORITE",
	"quartz" : "QUARTZ",
	"strass" : "RHINESTONE",
	"steenkristal" : "ROCKCRYSTAL",
	"rozenkwarts" : "ROSEQUARTZ",
	"robijn" : "RUBY",
	"saffier" : "SAPPHIRE",
	"leisteen" : "SLATE",
	"rookkwarts" : "SMOKYQUARTZ",
	"soladiet" : "SODALITE",
	"spinel" : "SPINEL",
	"laagsteen" : "STRATUMSTONE",
	"swarovski kristal" : "SWAROVSKICRYSTAL",
	"synthetisch" : "SYNTHETICSTONE",
	"tijgeroog" : "TIGEREYE",
	"topaas" : "TOPAZ",
	"toermalijn" : "TOURMALINE",
	"turquoise" : "TURQUOISE",
	"uvaroviet" : "UVAROVITE",
	"wildvinite" : "WILDVINITE",
	"xylopile" : "XYLOPILE",
	"zirkonia" : "ZIRCONIA"
}

const jewelType = {
	"enkelbandje" : "ANKLE_BRACELET",
	"armband" : "BRACELET",
	"broche" : "BROOCH",
	"?" : "CHARM",
	"choker" : "CHOKER",
	"combinatiering" : "COMBINATION_RING",
	"oorringen" : "CREOLE_EARRINGS",
	"manchetknopen" : "CUFFLINK",
	"oorhangers" : "HOOP_EARRINGS",
	"collier/ketting" : "NECKLACE",
	"hanger/bedel/charm" : "PENDANT",
	"ring" : "RING",
	"ring met edelsteen" : "RING_WITH_GEM",
	"ring met parel" : "RING_WITH_PEARL",
	"set" : "SET",
	"bangle armband" : "SLAVE_BRACELET",
	"oorstekers" : "STUD_EARRINGS",
	"tennisarmband" : "TENNIS_BRACELET",
	"tennis ketting" : "TENNIS_NECKLACE"
}

const Color = {
	"beige":"BEIGE",
	"zwart":"BLACK",
	"blauw":"BLUE",
	"brons":"BRASS",
	"bruin":"BROWN",
	"creme":"CREME",
	"goudkleurig":"GOLD_COLORED",
	"goud zilver kleurig":"GOLD_SILVER_COLORED",
	"groen":"GREEN",
	"grijs":"GREY",
	"parelmoer":"MOTHER_OF_PEARL",
	"parelmoer gekleurd":"MOTHER_OF_PEARL_COLOURED",
	"oranje":"ORANGE",
	"roze":"PINK",
	"paars":"PURPLE",
	"rood":"RED",
	"roségoudkleurig":"ROSE_GOLD_COLORED",
	"rosé zilver gekleurd":"ROSE_SILVER_COLORED",
	"zilverkleurig":"SILVER_COLORED",
	"taupe":"TAUPE",
	"wit":"WHITE",
	"geel":"YELLOW"
}

const jewelMaterial = {
	"goud" : "GOLD",
	"zilver" : "SILVER",
	"hout" : "WOOD",
	"canvas / nylon" : "CANVAS",
	"lederen bovenkant" : "UPPER_LEATHER",
	"pu leder" : "PU_LEATHER",
	"metaal" : "METAL",
	"aluminium" : "ALUMINIUM",
	"keramiek" : "CERAMICS",
	"edelstaal" : "STAINLESS_STEEL"
}

const strapMaterial = {
	"aluminium" : "ALUMINIUM",
	"echt leer" : "REAL_LEATHER",
	"lederen bovenkant" : "UPPER_LEATHER",
	"metaal" : "METAL",
	"kunststof / rubber" : "PLASTIC_SILICONE",
	"edelstaal" : "STAINLESS_STEEL",
	"titanium" : "TITANIUM",
	"keramiek" : "CERAMICS",
	"pu leder" : "PU_LEATHER",
	"canvas / nylon" : "CANVAS",
	"echt goud" : "REAL_GOLD"
}

const strapModel = {
	"rekband" : "EXPANDABLE_STRETCH_STRAP",
	"schakelketting zakhorloge" : "SELECT_BAND_FOR_POCKET_WATCH",
	"schakelband" : "SELECT_BAND",
	"milanees / mesh" : "MILANESE_MESH",
	"nato" : "NATO",
	"standaard model" : "STANDARD_MODEL"
}

const caseMaterial = {
   "titanium" : "TITANIUM",
	"edelstaal" : "STAINLESS_STEEL",
	"aluminium" : "ALUMINIUM",
	"metaal" : "METAL",
	"kunststof / rubber" : "PLASTIC_SILICONE",
	"hout" : "WOOD",
	"keramiek" : "CERAMICS"
}


const caseGlasstype = {
	"saffier" : "SAPPHIRE",
	"saffier gecoat" : "SAPPHIRE_COATED",
	"kristal" : "CRYSTAL",
	"mineraal" : "MINERAL",
	"kunststof / rubber" : "SYNTHETIC_PLASTIC",
}

const caseShape = {
	"ovaal" : "OVAL",
	"rechthoek" : "RECTANGLE",
	"rond" : "ROUND",
	"andere vorm" : "OTHER_SHAPE",
	"vierkant" : "SQUARE"
}

const dialIndex = {
	"cijfers (arabisch)" : "ARABIC",
	"puntjes" : "DOTS",
	"romeinse cijfers" : "ROMAN",
	"streepjes" : "STRIPES",
	"geen" : "NONE",
	"sierdiamanten (zirkonia / synthetisch)" : "ORNAMENTAL_DIAMONDS",
	"echte diamanten" : "REAL_DIAMONDS"
}

const dialPattern = {
    "puntjes":"DOTS",
    "streepjes":"STRIPES"
}

const watchIndication = {
	"analoog" : "ANALOG",
	"analoog en digitaal" : "ANALOG_DIGITAL",
	"chrono / multi" : "CHRONO_MULTI",
	"digitaal" : "DIGITAL",
}

const watchMovement = {
	"quartz" : "QUARTZ",
	"solar (zonne energie)" : "SOLAR",
	"gps solar" : "GPS_SOLAR",
	"kinetisch" : "KINETIC",
	"automatisch" : "AUTOMATIC",
	"handopwind" : "HAND_WINDING",
}

const watchType = {
	"smartwatch":"SMARTWATCH",
	"polshorloge":"WRIST",
	"verpleegsterhorloge":"NURSE",
	"zakhorloge":"POCKET"
}

const seikoSynonyms = [
	{"key":"caseGlasstype", "val":"hardlex mineraal glas", "equals":"MINERAL"},
	{"key":"caseGlasstype", "val":"saffier glas", "equals":"SAPHIRE"},
	{"key":"caseGlasstype", "val":"acryl glas", "equals":"SYNTHETIC_PLASTIC"},

	{"key":"caseMaterial", "val":"staal", "equals":"STAINLESS_STEEL"},
	{"key":"caseMaterial", "val":"kunststof", "equals":"PLASTIC_SILICONE"},
	{"key":"caseMaterial", "val":"hardcoated staal", "equals":"METAL"},

	{"key":"caseShape", "val":"tonneau", "equals":"OTHER_SHAPE"},
	{"key":"caseShape", "val":"vorm", "equals":"OTHER_SHAPE"},

	{"key":"strapMaterial", "val":"staal", "equals":"STAINLESS_STEEL"},
	{"key":"strapMaterial", "val":"leer", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"nylon", "equals":"CANVAS"},
	{"key":"strapMaterial", "val":"hardcoated staal", "equals":"METAL"},
	{"key":"strapMaterial", "val":"urethaan", "equals":"PLASTIC_SILICONE"},
	{"key":"strapMaterial", "val":"silicoon", "equals":"PLASTIC_SILICONE"},
	{"key":"strapMaterial", "val":"siliconen", "equals":"PLASTIC_SILICONE"},

	{"key":"Color", "val":"rose", "equals":"ROSE_GOLD_COLORED"},
	{"key":"Color", "val":"tweekleurig", "equals":"GOLD_SILVER_COLORED"},
	{"key":"Color", "val":"zilver", "equals":"SILVER_COLORED"},
	{"key":"Color", "val":"champagne", "equals":"CREME"},	

	{"key":"watchIndication", "val":"chronograaf", "equals":"CHRONO_MULTI"},
	{"key":"watchMovement", "val":"solar", "equals":"SOLAR"},
	{"key":"watchMovement", "val":"automaat", "equals":"AUTOMATIC"},
	{"key":"watchMovement", "val":"kinetic", "equals":"KINETIC"}
]

const seikoArticleOverrides = [
	{"sku":"RT381AX9", "attr":"Color", "of":"KLEURWIJZERPLAAT", "equals":"BROWN"},
	{"sku":"RXN31DX9", "attr":"Color", "of":"KLEURWIJZERPLAAT", "equals":"BROWN"},
	{"sku":"RRX49EX9", "attr":"Color", "of":"KLEURWIJZERPLAAT", "equals":"PINK"},
	{"sku":"R2357AX9", "attr":"Color", "of":"KLEURWIJZERPLAAT", "equals":"GREY"},
	{"sku":"R2371AX9", "attr":"Color", "of":"KLEURWIJZERPLAAT", "equals":"GREY"},
	{"sku":"R2351AX9", "attr":"Color", "of":"KLEURWIJZERPLAAT", "equals":"GREY"},
	{"sku":"R2361AX-9", "attr":"Color", "of":"KLEURWIJZERPLAAT", "equals":"GREY"},
	{"sku":"R2357AX9", "attr":"Color", "of":"KLEURWIJZERPLAAT", "equals":"GREY"},
	{"sku":"RG281TX9", "attr":"Color", "of":"KLEURWIJZERPLAAT", "equals":"WHITE"},
	{"sku":"RRX49EX9", "attr":"Color", "of":"KLEURKAST", "equals":"PINK"}
]