# BBS API Connection

## Contact
frans@bbs-shop.com

## Authentication
1. Send a POST-request with the following details:

```bash
curl -X POST "https://www.bbs-shop.com/api/auth/get_tokens?username=BBS@API&password=productfeed&db=BBS"

```
2. The response contains an **access_token**, use this token when calling the API for product

3. Send a GET-request to `https://www.bbs-shop.com/api/product.template` with these headers:
```
{
	"access-token":"<the access token>",
	"email":"data@prismanote.com"
}
```
4. This will return a JSON

```python

import requests
import json
import datetime

auth_url = "https://www.bbs-shop.com/api/auth/get_tokens?username=BBS@API&password=productfeed&db=BBS"
products_url = "https://www.bbs-shop.com/api/product.template"

now = str(datetime.datetime.now().timestamp()).split(".")[0]
auth = requests.post(auth_url)

if auth.status_code == 200:
	res = json.loads(auth.text)
	if 'access_token' in res['data']:
		token = res['data']['access_token']
		products = requests.get(url=products_url, headers = {"access-token":token,"email":"data@prismanote.com"})
		if products.status_code == 200:
			with open(str(now)+".json", "w") as f1:
				f1.write(products.text)
			print('Saved to file!')
		else:
			print('invalid product request..')
	else:
		print('no access-token in auth response')
else:
	print('invalid auth request..')

```