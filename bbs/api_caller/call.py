import requests
import json
import datetime

now = str(datetime.datetime.now().timestamp()).split(".")[0]
auth = requests.post("https://www.bbs-shop.com/api/auth/get_tokens?username=BBS@API&password=productfeed&db=BBS")

if auth.status_code == 200:
	res = json.loads(auth.text)
	if 'access_token' in res['data']:
		token = res['data']['access_token']
		products = requests.get(url="https://www.bbs-shop.com/api/product.template", headers = {"access-token":token, "email":"data@prismanote.com"})
		if products.status_code == 200:
			with open(str(now)+".json", "w") as f1:
				f1.write(products.text)
			print('Saved to file!')
		else:
			print('invalid product request..')
	else:
		print('no access-token in auth response')
else:
	print('invalid auth request..')