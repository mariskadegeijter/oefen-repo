'use strict';
const fs = require('fs');

var messages = []
var descriptions = []

function _removeAccents(strAccents) {
	if(isNaN(strAccents)) {
		strAccents = strAccents.split('');
		var strAccentsOut = new Array();
		var strAccentsLen = strAccents.length;
		var accents =    "ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž";
		var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
		for (var y = 0; y < strAccentsLen; y++) {
			if (accents.indexOf(strAccents[y]) != -1) {
				strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
			} else
				strAccentsOut[y] = strAccents[y];
		}
		strAccentsOut = strAccentsOut.join('');
		return strAccentsOut;
	} else {
		return false
	}
}

function _lower(str) {
	return str.toLowerCase()
}

function _chkVal(val) {
	if(Array.isArray(val)) {
		return Boolean(val.length > 0 && val[0] !== "")
	} else {
		return Boolean(val)
	}
}

function _chkValFalse(val) {
	if(Array.isArray(val)) {
		return Boolean(val.length > 0 && val[0] !== "" && val[0] !== false)
	} else {
		return Boolean(val)
	}
}

function _convertStripe(str) {
	return String(str).replace("_","-")
}

function _convertMultipleStripes(str){
	return _convertStripe(_convertStripe(_convertStripe(_convertStripe(_convertStripe(str)))))
}

function _isDefined(arr) {
	return arr !== undefined ? arr : false
}

function containsNumber(str) {
	return /\d/.test(str);
}

function noWhitespace(str) {
	return String(str).replace(/\s/g, '')
} 


//assigns the right values from the properties to the final article

function parseStrapProperties(article,art,currentProductNumber) {
	article.category = 'STRAP'

	if(_chkValFalse(art['product_size'])) {
		article.strap.width = art.product_size
		article.size = art.product_size
	}

	if(_chkValFalse(art.leather_type)) {
		article.strap.material = getPrismaNoteProperty('strapMaterial', art['leather_type'][1], "leather_type", currentProductNumber, art['leather_type'][1])
	}

	if(_chkValFalse(art.product_color)) {
		article.strap.color = getPrismaNoteProperty('Color', art['product_color'][1], "product_color", currentProductNumber, art['product_color'][1])
	}

		//type sluiting
	if(_chkValFalse(art.description_sale)) {
		if(art.description_sale.includes('clasp not included')) {			
		} else if(art.description_sale.includes('dubbele vouwsluiting of vlinder sluiting')) {			
		} else if(art.description_sale.includes('dubbele vouwsluiting')) {
			article.strap.clasp = "dubbele vouwsluiting"
		} else if(art.description_sale.includes('vouwsluiting met beveiliging')) {
			article.strap.clasp = "vouwsluiting met beveiliging"
		} else if(art.description_sale.includes('vouwsluiting')) {
			article.strap.clasp = "vouwsluiting"
		} else if(art.description_sale.includes('gesp met dubbele doorn')) {
			article.strap.clasp = 'gesp met dubbele doorn'
		} else if(art.description_sale.includes('gesp ')) {
			article.strap.clasp = 'gesp'
		} else if(art.description_sale.includes('beveiligingssluiting')) {
			article.strap.clasp = 'beveiligingssluiting'
		}
	}	
		//bandmodel
	if(_chkValFalse(art['description_sale'])){
		if(art['description_sale'].toLowerCase().includes('rekband')) {
			article.strap.model = "EXPANDABLE_STRETCH_STRAP"
		} else if(art['description_sale'].toLowerCase().includes('milanese')) {
			article.strap.model = "MILANESE_MESH"
		} else if(art['description_sale'].toLowerCase().includes('nato')) {
			article.strap.model = "NATO"
		} else if(art['description_sale'].toLowerCase().includes('link')) {
			article.strap.model = "SELECT_BAND"
		} else if(_chkValFalse(art.leather_type)){
			if(article.strap.material === "REAL_LEATHER" || article.strap.material === "PLASTIC_SILICONE") {
			article.strap.model = 'STANDARD_MODEL'
			}
		}
	}
	
	return article
}

function parseWatchProperties(article,art,currentProductNumber){
	article.category = "WATCH"

	if(_chkValFalse(art['leather_type'])) {
		article.watch.strap.material = getPrismaNoteProperty('strapMaterial', art['leather_type'][1], "leather_type", currentProductNumber, art['leather_type'][1])
	}	

	if(_chkValFalse(art['leather_type']) && article.watch.strap.material == "TITANIUM") {
		article.watch.isAntiAllergy = true
		article.watch.isNickelFree = true
	}

	if(_chkValFalse(art['collection_style']) && _chkValFalse(art['barcode']) && art['collection_style'][1] == "Smartwatch") {
		article.watch.isSmartWatch = true
		article.watch.hasDateFunction = true
		article.watch.hasLigtfunction = true
		article.watch.type = 'SMARTWATCH'
		article.watch.indication = 'DIGITAL'
	} else {
		article.watch.type = 'WRIST'
	}

	if(_chkValFalse(art['product_color'])) {
		article.watch.strap.color = getPrismaNoteProperty('Color', art['product_color'][1], "product_color", currentProductNumber, art['product_color'][1])
	}

	if(_chkValFalse(art['description_sale'])) {
		if(art.description_sale.includes('clasp not included')) {			
		} else if(art.description_sale.includes('dubbele vouwsluiting of vlinder sluiting')) {			
		} else if(art.description_sale.includes('dubbele vouwsluiting')) {
			article.watch.strap.clasp = "dubbele vouwsluiting"
		} else if(art.description_sale.includes('vouwsluiting met beveiliging')) {
			article.watch.strap.clasp = "vouwsluiting met beveiliging"
		} else if(art.description_sale.includes('vouwsluiting')) {
			article.watch.strap.clasp = "vouwsluiting"
		} else if(art.description_sale.includes('gesp met dubbele doorn')) {
			article.watch.strap.clasp = 'gesp met dubbele doorn'
		} else if(art.description_sale.includes('gesp ')) {
			article.watch.strap.clasp = 'gesp'
		} else if(art.description_sale.includes('beveiligingssluiting')) {
			article.watch.strap.clasp = 'beveiligingssluiting'
		}
	}

		//Bandmodel
	if(_chkValFalse(art['description_sale']) && art['description_sale'].toLowerCase().includes('rekband')) {
		article.watch.strap.model = "EXPANDABLE_STRETCH_STRAP"
	} else if(_chkValFalse(art['description_sale'])) {
		if(art['description_sale'].toLowerCase().includes('milanese')) {
			article.watch.strap.model = "MILANESE_MESH"
		} else if(art['description_sale'].toLowerCase().includes('nato')) {
			article.watch.strap.model = "NATO"
		} else if(art['description_sale'].toLowerCase().includes('link')) {
			article.watch.strap.model = "SELECT_BAND"
		}
	} else if(_chkValFalse(art['leather_type'])) {
		if(article.watch.strap.material === "REAL_LEATHER" || article.watch.strap.material === "PLASTIC_SILICONE") {
			article.watch.strap.model = "STANDARD_MODEL"
		}
	} 

	if(_chkValFalse(art['product_size'])) {
		article.watch.strap.width = art['product_size']
		article.variants.size = art['product_size']
	}

	return article
}

fs.readFile('./src/bbs-api-output-NL.json','utf8', function(errors,data) {	
	if(data) {
		var jsonData = JSON.parse(data)

		console.log(errors)
		//console.log(data)

		if(process.argv.length < 3) {
			console.log("This is a dry run, the output is not saved.")
			console.log("Run this script as 'node bbs.js --save' to save the output.")
		}

		if(jsonData) {
			init(jsonData);
		} else {
			throw new Error("Invalid JSON input")
		}
	} else {
		throw new Error("No JSON input")
	}
});

function bbsParser(srcData) {
	let productsFound = []
	let known_keys = []
	let known_types = []
	let sizes = []

	if(srcData.data && srcData.data.results) {

		let articles = srcData.data.results
		
		articles.forEach(function(art) {
			let article = {
				"images":[],
				"variants": [],
				"hasStock":true,
				"size":0,
				"weight":0,
				"totalCaratWeight":0,
				//"gems":[
					// {
					// 	"quantity":0,
					// 	"gemKind":"",
					// 	"gemPurity":"",
					// 	"gemColor":"",
					// 	"gemcut":"",
					// 	"caratWeight":0,
					// 	"suggestedRetailPriceDiamond":0
					// }
				//],
				"jewel":{
					"material":"",
					"color":"",
					"type":"",
					"height":0,
					"width":0,
					"depth":0,
					"diameter":0,
					"weight":0,
					"chain":"",
					"clasp":"",
					"shape":"",
					"gloss":false
				},
				"strap":{
					"model":"",
					"width":0,
					"material":"",
					"color":"",
					"print":"",
					"studs":"",
					"clasp":"",
					"pattern":"",
					"wristPermimeter":""
				},
				"watch":{
					"type":"",
					"movement":"",
					"indication":"",
					"hasSwissMovement":false,
					"hasDateFunction":false,
					"waterProofLevel":null,
					"isNickelFree":false,
					"isAntiAllergy":false,
					"hasLightFunction":false,
					"isSmartWatch":false,
					"smartWatchFunctions":"",
					"dial":{
						"color":"",
						"pattern":"",
						"print":"",
						"index":""
					},
					"case":{
						"shape":"",
						"size":0,
						"depth":0,
						"material":"",
						"glassMaterial":"",
						"color":""
					},
					"strap":{
						"model":"",
						"width":0,
						"material":"",
						"color":"",
						"print":"",
						"studs":"",
						"clasp":"",
						"pattern":"",
						"wristPermimeter":""
					},
				},
				"category":"",
				"brand":{
					"name":"",
				},
				"suggestedRetailPrice":0,
				"suggestedRetailPriceVat":0,
				"male":true,
				"female":true,
				"kids":false,
				"nl":{
					"name":"",
					"shortDescription":"",
					"longDescription":""
				},
				"en":{
					"name":"",
					"shortDescription":"",
					"longDescription":""
				},
				"de":{
					"name":"",
					"shortDescription":"",
					"longDescription":""
				},
				"uploader": {
                "_id": "",
                "name": "BBS"
                }
			}

			let nametoLowerCase = art['name'].toLowerCase()

			let currentProductNumber = ""
			if(art['list_price'] < 1.05){
				currentProductNumber = ('BBS-'+art['id'])
			} else if( containsNumber(art['name']) === false){
			 	currentProductNumber = ('BBS-'+art['id'])
			} else if(
				nametoLowerCase.includes('watch') || 
				nametoLowerCase.includes('strap') || 
				nametoLowerCase.includes('bbs') || 
				nametoLowerCase.includes('boek') ||
				nametoLowerCase.includes('book') ||
				nametoLowerCase.includes('masks')||
				nametoLowerCase.includes('stunt')){
				currentProductNumber = ('BBS-'+art['id'])
			} else if(_chkValFalse(art['name'])){
				currentProductNumber = _convertMultipleStripes(noWhitespace(art['name'])).toUpperCase().trim()
			}

			let currentEAN = _chkValFalse(art['barcode']) ? art['barcode'] : ""
			
			let variant = {
				"productNumberAuto":false,
				"size":art.product_size,
				"productNumber":currentProductNumber,
				"insertedProductNumber":currentProductNumber,
				"ean":currentEAN,
				"shopStratingDigits":""
			}

			article.variants.push(variant)

			let collection_style = (art['collection_style'] !== undefined && art['collection_style'][1] !== undefined) ? art['collection_style'][1].toLowerCase() : ""

			article.category = 'OTHER'

			//set category
			if(collection_style && collection_style === "smartwatch"){
				article.category = 'WATCH'
			} else if(_chkValFalse(art['name']) && _chkValFalse(art['barcode']) && art['name'].includes('atl')) {
				article.category = 'WATCH'
			} else if(_chkValFalse(art['description_sale']) && art['description_sale'].toLowerCase().includes('watch strap')) {
				article.category = 'STRAP'
			} else if(_chkValFalse(art['description_sale']) && art['description_sale'].toLowerCase().includes('horlogeband')) {
				article.category = 'STRAP'
			}
			
			//set image
			if(_chkValFalse(art['image1']) && _chkVal(art['name'])) {
				article.images.push({
				src: art['image1'] ,
				alt: art['name'] ? art['name'] : currentProductNumber
				})
			} else {
				if(!_chkVal(art['image1'])) { messages.push({"product": currentProductNumber, "problem":"has no image url"}) }
				if(!_chkVal(art['name'])) { messages.push({"product": art['id'], "problem":"has no description as for image alt"}) }
			}
			if(_chkValFalse(art['image2']) && _chkVal(art['name'])) {
				article.images.push({
				src: art['image2'] ,
				alt: art['name'] ? art['name'] : currentProductNumber
				})
			}
			if(_chkValFalse(art['image3']) && _chkVal(art['name'])) {
				article.images.push({
				src: art['image3'] ,
				alt: art['name'] ? art['name'] : currentProductNumber
				})
			}
			
			//set name
			if(_chkValFalse(art['name'])) {
				if(article.category === "WATCH") { 
					article.nl.name = String('bbs horloge '+currentProductNumber).toUpperCase()
					article.en.name = String('bbs watch '+currentProductNumber).toUpperCase()
					article.de.name = String('bbs uhr '+currentProductNumber).toUpperCase()
				} else if(article.category === "STRAP") {
					article.nl.name = String('bbs horlogeband '+currentProductNumber).toUpperCase()
					article.en.name = String('bbs watch strap '+currentProductNumber).toUpperCase()
					article.de.name = String('bbs uhrenarmband '+currentProductNumber).toUpperCase()
				} else if(article.category === "OTHER" && art['collection_style'].length > 1 && art['collection_style'][1].toLowerCase().includes('Tools')) {
					article.nl.name = String('bbs gereedschap '+currentProductNumber).toUpperCase()
					article.en.name = String('bbs tools '+currentProductNumber).toUpperCase()
					article.de.name = String('bbs werkzeuge '+currentProductNumber).toUpperCase()
				} else if(article.category === "OTHER") {

					let isBattery = false

					let leather_type = (art['leather_type'] !== undefined && art['leather_type'][1] !== undefined) ? art['leather_type'][1].toLowerCase() : ""

					if(art['name'].includes('Rayovac') || art['name'].includes('Maxell')) {
						isBattery = true
					} else {
						if(leather_type) {
							if(leather_type.includes('lithium') || leather_type.includes('kwik vrij') || leather_type.includes('alkaline')) {
								isBattery = true
							}
						}
					}

					if(isBattery) {
						article.nl.name = String('bbs batterij '+currentProductNumber).toUpperCase()
						article.en.name = String('bbs battery '+currentProductNumber).toUpperCase()
						article.de.name = String('bbs batterie '+currentProductNumber).toUpperCase()
					}

				} else{
					let articleName = String('bbs '+currentProductNumber).toUpperCase()
					article.nl.name = articleName
					article.en.name = articleName
					article.de.name = articleName
				}
			}
			
			//set brandname		
			article.brand.name = "BBS"

			//set price
			if(_chkVal(art['rrp'])) { article.suggestedRetailPrice = parseFloat(art['rrp'])/1.21}
			if(_chkVal(art['rrp'])) { article.suggestedRetailPriceVat = 21}

			if(_chkVal(art['description_sale'])){
				article.nl.shortDescription = art['description_sale']	
			}

			if(article.category === 'STRAP'){
				article = parseStrapProperties(article,art,currentProductNumber)
				delete article.watch
				delete article.jewel
			} else if (article.category === 'WATCH') {
				article = parseWatchProperties(article,art,currentProductNumber)
				delete article.jewel
				delete article.strap
			} else {
				delete article.jewel
				delete article.strap
				delete article.watch
			}

			productsFound.push(article)
			console.log(currentProductNumber)
		});
	} else {
		throw new Error("No articles in source data")
	}

	console.log('finished')
	console.warn('PLEASE CHECK IF VAT 21% SHOULD ALWAYS BE APPLIED!')
	console.warn('CHECK IF LIST_PRICE OR RRP IS USED')

	let problems = messages.filter(m => { return m.problem && m.problem !== '' });
	let warnings = messages.filter(m => { return m.warning && m.warning !== '' });
	let notifications = messages.filter(m => { return m.notification && m.notification !== '' });

	console.log("Problems: ",problems.length)
    console.log("Warnings: ",warnings.length)
    console.log("Notifications: ",notifications.length)

	return {
		"data":productsFound,
		"problems":problems,
		"warnings":warnings,
		"notifications":notifications
	};
}

function getPrismaNoteProperty(type,val,srcProp,productNumber=null,material) {

	val = _removeAccents(val)

	val = (typeof val === 'string') ? val.toLowerCase() : val

	if(![
		"caseGlasstype",
		"caseMaterial",
		"caseShape",
		"Color",
		"dialIndex",
		"dialPattern",
		"gemKind ",
		"jewelMaterial",
		"jewelType",
		"strapMaterial",
		"strapModel",
		"watchIndication",
		"watchMovement",
		"watchType"
		].includes(type)) {
		return null
	}

	let res = {
		"success":false,
		"data":"",
		"errors":[]
	}

	let targetProp = null

	switch(type) {

		case "caseGlasstype":
			targetProp = caseGlasstype
		break;
		case "caseMaterial":
			targetProp = caseMaterial	
		break;
		case "caseShape":
			targetProp = caseShape		
		break;
		case "Color":
			targetProp = Color
		break;
		case "dialIndex":
			targetProp = dialIndex
		break;
		case "dialPattern":
			targetProp = dialPattern
		break;
		case "gemKind ":
			targetProp = gemKind	
		break;
		case "jewelMaterial":
			targetProp = jewelMaterial	
		break;
		case "jewelType":
			targetProp = jewelType
		break;
		case "strapMaterial":
			targetProp = strapMaterial
		break;
		case "strapModel":
			targetProp = strapModel
		break;
		case "watchIndication":
			targetProp = watchIndication
		break;
		case "watchMovement":
			targetProp = watchMovement
		break;
		case "watchType":
			targetProp = watchType
		break;

		default:
			targetProp = null
		break;
	}

	if(_chkVal(targetProp)) {
		if(val in targetProp) { //there's an exact match
			res.success = true
			res.data = targetProp[val]
		} else { //there's no match, so let's try one of the synonyms
			
			synonyms.forEach(s => {
				if(type === s['key'] && val === s['val']) {
					res.success = true
					res.data = s['equals']
					messages.push({
						"product":(productNumber ? productNumber : "(no productnumber)"),
						"notification":String(type+" '"+val+"' - alias found: "+s['equals'])
					})
				}
			});

			// if(!res.success) {
			// 	overrides.forEach(s => {
			// 		if(s['attr'] === type && s['of'] === srcProp && s['sku'] === productNumber) {
			// 			res.data = s['equals']
			// 			res.success = true
			// 			messages.push({"product":(productNumber ? productNumber : "(no productnumber)"),"notification":String(type+" '"+val+"' overwritten by "+s['equals'])})
			// 		}
			// 	});

					if(!res.success) {
						console.log(String("Not found: "+productNumber+", "+srcProp+": "+val+" ("+type+")"))
					}
				//}
			//}
		}
	}

	if(res.success) {
		return res.data
	} else {
		messages.push({
			"product":(productNumber ? productNumber : "(no productnumber)"),
			"problem":String(type+" '"+val+"' not found for "+(srcProp ? srcProp : ""))
		})
		return;
	}
}




const gemKind = {
	"agaat" : "AGATE",
	"amazone" : "AMAZONE",
	"amazoniet" : "AMAZONITE",
	"barnsteen" : "AMBER",
	"amethyst" : "AMETHYST",
	"aquamarijn" : "AQUAMARINE",
	"aventurijn" : "AVENTURINE",
	"beryl" : "BERYLL",
	"bloedkoraal" : "BLOODCORAL",
	"calciet" : "CALCITE",
	"cameo" : "CAMEO",
	"carneool" : "CARNELEAN",
	"cateye" : "CATEYE",
	"keramiek" : "CERAMICS",
	"chalcedoon" : "CHALCEDONY",
	"citrien" : "CITRINE",
	"korund" : "CORUNDUM",
	"kristal" : "CRYSTAL",
	"diamant" : "DIAMOND",
	"smaragd" : "EMERALD",
	"glazuur" : "ENAMEL",
	"epidote" : "EPIDOTE",
	"firepost" : "FIREPOST",
	"fluoriet" : "FLUORITE",
	"granaat" : "GARNET",
	"glas" : "GLASS",
	"heliotroop" : "HELIOTROPE",
	"hematiet" : "HEMATITE",
	"iriskwarts" : "IRISQUARTZ",
	"jade" : "JADE",
	"jargon" : "JARGON",
	"jasper" : "JASPER",
	"labradoriet" : "LABRADORITE",
	"lapis" : "LAPIS",
	"lazuli" : "LAZULI",
	"lazuriet" : "LAZURITE",
	"citroenkwarts" : "LEMONQUARTZ",
	"madeiracitrine" : "MADEIRACITRINE",
	"malachiet" : "MALACHITE",
	"maansteen" : "MOONSTONE",
	"muranoglass" : "MURANOGLASS",
	"natuursteen" : "NATURALSTONE",
	"nefriet" : "NEPHRITE",
	"onyx" : "ONYX",
	"opaal" : "OPAL",
	"parel" : "PEARL",
	"parel akoya" : "PEARLAKOYA",
	"parel cultive" : "PEARLCULTIVE",
	"parel zuidzee" : "PEARLSOUTHSEA",
	"parel zoetwater" : "PEARLSWEETWATER",
	"parel synthetisch" : "PEARLSYNTHETICAL",
	"parel tahiti" : "PEARLTAHITI",
	"peridoot" : "PERIDOTE",
	"fosforiet" : "PHOSPHORITE",
	"quartz" : "QUARTZ",
	"strass" : "RHINESTONE",
	"steenkristal" : "ROCKCRYSTAL",
	"rozenkwarts" : "ROSEQUARTZ",
	"robijn" : "RUBY",
	"saffier" : "SAPPHIRE",
	"leisteen" : "SLATE",
	"rookkwarts" : "SMOKYQUARTZ",
	"soladiet" : "SODALITE",
	"spinel" : "SPINEL",
	"laagsteen" : "STRATUMSTONE",
	"swarovski kristal" : "SWAROVSKICRYSTAL",
	"synthetisch" : "SYNTHETICSTONE",
	"tijgeroog" : "TIGEREYE",
	"topaas" : "TOPAZ",
	"toermalijn" : "TOURMALINE",
	"turquoise" : "TURQUOISE",
	"uvaroviet" : "UVAROVITE",
	"wildvinite" : "WILDVINITE",
	"xylopile" : "XYLOPILE",
	"zirkonia" : "ZIRCONIA"
}

const jewelType = {
	"enkelbandje" : "ANKLE_BRACELET",
	"armband" : "BRACELET",
	"broche" : "BROOCH",
	"?" : "CHARM",
	"choker" : "CHOKER",
	"combinatiering" : "COMBINATION_RING",
	"oorringen" : "CREOLE_EARRINGS",
	"manchetknopen" : "CUFFLINK",
	"oorhangers" : "HOOP_EARRINGS",
	"collier/ketting" : "NECKLACE",
	"hanger/bedel/charm" : "PENDANT",
	"ring" : "RING",
	"ring met edelsteen" : "RING_WITH_GEM",
	"ring met parel" : "RING_WITH_PEARL",
	"set" : "SET",
	"bangle armband" : "SLAVE_BRACELET",
	"oorstekers" : "STUD_EARRINGS",
	"tennisarmband" : "TENNIS_BRACELET",
	"tennis ketting" : "TENNIS_NECKLACE"
}

const jewelMaterial = {
	"goud" : "GOLD",
	"zilver" : "SILVER",
	"hout" : "WOOD",
	"canvas / nylon" : "CANVAS",
	"lederen bovenkant" : "UPPER_LEATHER",
	"pu leder" : "PU_LEATHER",
	"metaal" : "METAL",
	"aluminium" : "ALUMINIUM",
	"keramiek" : "CERAMICS",
	"edelstaal" : "STAINLESS_STEEL"
}

const Color = {
	"beige":"BEIGE",
	"zwart":"BLACK",
	"blauw":"BLUE",
	"brons":"BRASS",
	"bruin":"BROWN",
	"creme":"CREME",
	"goudkleurig":"GOLD_COLORED",
	"goud zilver kleurig":"GOLD_SILVER_COLORED",
	"groen":"GREEN",
	"grijs":"GREY",
	"parelmoer":"MOTHER_OF_PEARL",
	"parelmoer gekleurd":"MOTHER_OF_PEARL_COLOURED",
	"oranje":"ORANGE",
	"roze":"PINK",
	"paars":"PURPLE",
	"rood":"RED",
	"roségoudkleurig":"ROSE_GOLD_COLORED",
	"rosé zilver gekleurd":"ROSE_SILVER_COLORED",
	"zilverkleurig":"SILVER_COLORED",
	"taupe":"TAUPE",
	"wit":"WHITE",
	"geel":"YELLOW"
}

const strapMaterial = {
	"aluminium" : "ALUMINIUM",
	"echt leer" : "REAL_LEATHER",
	"lederen bovenkant" : "UPPER_LEATHER",
	"metaal" : "METAL",
	"kunststof / rubber" : "PLASTIC_SILICONE",
	"edelstaal" : "STAINLESS_STEEL",
	"titanium" : "TITANIUM",
	"keramiek" : "CERAMICS",
	"pu leder" : "PU_LEATHER",
	"canvas / nylon" : "CANVAS",
	"echt goud" : "REAL_GOLD"
}

const strapModel = {
	"rekband" : "EXPANDABLE_STRETCH_STRAP",
	"schakelketting zakhorloge" : "SELECT_BAND_FOR_POCKET_WATCH",
	"schakelband" : "SELECT_BAND",
	"milanees / mesh" : "MILANESE_MESH",
	"nato" : "NATO",
	"standaard model" : "STANDARD_MODEL"
}

const caseMaterial = {
	"titanium" : "TITANIUM",
	"edelstaal" : "STAINLESS_STEEL",
	"aluminium" : "ALUMINIUM",
	"metaal" : "METAL",
	"kunststof / rubber" : "PLASTIC_SILICONE",
	"hout" : "WOOD",
	"keramiek" : "CERAMICS"
}


const caseGlasstype = {
	"saffier" : "SAPPHIRE",
	"saffier gecoat" : "SAPPHIRE_COATED",
	"kristal" : "CRYSTAL",
	"mineraal" : "MINERAL",
	"kunststof / rubber" : "SYNTHETIC_PLASTIC",
}

const caseShape = {
	"ovaal" : "OVAL",
	"rechthoek" : "RECTANGLE",
	"rond" : "ROUND",
	"andere vorm" : "OTHER_SHAPE",
	"vierkant" : "SQUARE"
}

const dialIndex = {
	"cijfers (arabisch)" : "ARABIC",
	"puntjes" : "DOTS",
	"romeinse cijfers" : "ROMAN",
	"streepjes" : "STRIPES",
	"geen" : "NONE",
	"sierdiamanten (zirkonia / synthetisch)" : "ORNAMENTAL_DIAMONDS",
	"echte diamanten" : "REAL_DIAMONDS"
}

const dialPattern = {
    "puntjes":"DOTS",
    "streepjes":"STRIPES"
}

const watchIndication = {
	"analoog" : "ANALOG",
	"analoog en digitaal" : "ANALOG_DIGITAL",
	"chrono / multi" : "CHRONO_MULTI",
	"digitaal" : "DIGITAL",
}

const watchMovement = {
	"quartz" : "QUARTZ",
	"solar (zonne energie)" : "SOLAR",
	"gps solar" : "GPS_SOLAR",
	"kinetisch" : "KINETIC",
	"automatisch" : "AUTOMATIC",
	"handopwind" : "HAND_WINDING",
}

const watchType = {
	"smartwatch":"SMARTWATCH",
	"polshorloge":"WRIST",
	"verpleegsterhorloge":"NURSE",
	"zakhorloge":"POCKET"
}

const colorSpellingFix = {
	"Rood":"rode",
	"Zilverkleurig":"zilver",
	"Turquoise":"turquoise",
	"Oranje":"oranje",
	"Zwart":"zwarte",
	"Paars":"paarse",
	"Wit":"witte",
	"Blauw":"blauwe",
	"Roze":"roze",
	"Bruin":"bruine",
	"Grijs":"grijze",
	"Groen":"groene",
	"Champagne":"crèmekleurige"
}

const synonyms = [
	{"key":"jewelType", "val":"ringen", "equals":"RING"},
	{"key":"jewelType", "val":"oorbellen", "equals":"HOOP_EARRINGS"},
	{"key":"jewelType", "val":"kettingen", "equals":"NECKLACE"},
	{"key":"jewelType", "val":"armbanden", "equals":"BRACELET"},
	{"key":"jewelType", "val":"hangers", "equals":"PENDANT"},
	{"key":"jewelType", "val":"sluitingen", "equals":"CHARM"},

	{"key":"jewelMaterial", "val":"zilver, geel verguld", "equals":"SILVER"},
	{"key":"jewelMaterial", "val":"zilver, rose verguld", "equals":"SILVER"},
	{"key":"jewelMaterial", "val":"zilver met leer", "equals":"SILVER"},
	{"key":"jewelMaterial", "val":"zilver met koord", "equals":"SILVER"},
	{"key":"jewelMaterial", "val":"14 karaat goud", "equals":"GOLD"},
	{"key":"jewelMaterial", "val":"staal", "equals":"STAINLESS_STEEL"},
	{"key":"jewelMaterial", "val":"staal geel vergulde kleur", "equals":"STAINLESS_STEEL"},
	{"key":"jewelMaterial", "val":"staal rose vergulde kleur", "equals":"STAINLESS_STEEL"},

	{"key":"Color", "val":"14 karaat goud", "equals":"GOLD"},
	{"key":"Color", "val":"champagne", "equals":"CREME"},	
	{"key":"Color", "val":"turquoise", "equals":"BLUE"},
	{"key":"Color", "val":"rosegoudkleurig", "equals":"ROSE_GOLD_COLORED"},
	{"key":"Color", "val": 1, "equals":"BLACK"},
	{"key":"Color", "val": 2, "equals":"BLUE"},
	{"key":"Color", "val": 3, "equals":"BROWN"},
	{"key":"Color", "val": 4, "equals":"BROWN"},
	{"key":"Color", "val": 5, "equals":"RED"},
	{"key":"Color", "val": 6, "equals":"GREEN"},
	{"key":"Color", "val": 7, "equals":"BROWN"},
	{"key":"Color", "val": 8, "equals":"GREEN"},
	{"key":"Color", "val": 9, "equals":"WHITE"},
	{"key":"Color", "val": 10, "equals":"BROWN"},
	{"key":"Color", "val": 11, "equals":"RED"},
	{"key":"Color", "val": 12, "equals":"BLUE"},
	{"key":"Color", "val": 13, "equals":"PURPLE"},
	{"key":"Color", "val": 14, "equals":"GREEN"},
	{"key":"Color", "val": 15, "equals":"BLUE"},
	{"key":"Color", "val": 16, "equals":"ORANGE"},
	{"key":"Color", "val": 18, "equals":"ORANGE"},
	{"key":"Color", "val": 19, "equals":"GREY"},
	{"key":"Color", "val": 20, "equals":"SILVER_COLORED"},
	{"key":"Color", "val": 21, "equals":"YELLOW"},
	{"key":"Color", "val": 22, "equals":"SILVER_COLORED"},
	{"key":"Color", "val": 23, "equals":"GREY"},
	{"key":"Color", "val": 28, "equals":"BLUE"},
	{"key":"Color", "val": 30, "equals":""},
	{"key":"Color", "val": 33, "equals":""},
	{"key":"Color", "val": 40, "equals":"GOLD_COLORED"},
	{"key":"Color", "val": 42, "equals":"BROWN"},
	{"key":"Color", "val": 44, "equals":"GOLD_COLORED"},
	{"key":"Color", "val": 46, "equals":"YELLOW"},
	{"key":"Color", "val": 50, "equals":"SILVER_COLORED"},
	{"key":"Color", "val": 54, "equals":"ROSE_GOLD_COLORED"},
	{"key":"Color", "val": 60, "equals":"SILVER_COLORED"},
	{"key":"Color", "val": 64, "equals":"YELLOW"},
	{"key":"Color", "val": 65, "equals":"BROWN"},
	{"key":"Color", "val": "01 zwart", "equals":"BLACK"},
	{"key":"Color", "val": "02 marine blauw", "equals":"BLUE"},
	{"key":"Color", "val": "03 donker bruin", "equals":"BROWN"},
	{"key":"Color", "val": "04 tarwe", "equals":"BROWN"},
	{"key":"Color", "val": "05 bordeaux rood", "equals":"RED"},
	{"key":"Color", "val": "06 donker groen", "equals":"GREEN"},
	{"key":"Color", "val": "07 licht bruin", "equals":"BROWN"},
	{"key":"Color", "val": "08 kaki", "equals":"GREEN"},
	{"key":"Color", "val": "09 wit", "equals":"WHITE"},
	{"key":"Color", "val": "10 choco bruin", "equals":"BROWN"},
	{"key":"Color", "val": "11 rood", "equals":"RED"},
	{"key":"Color", "val": "12 pastel blauw", "equals":"BLUE"},
	{"key":"Color", "val": "13 paars", "equals":"PURPLE"},
	{"key":"Color", "val": "14 appel groen", "equals":"GREEN"},
	{"key":"Color", "val": "15 hard blauw", "equals":"BLUE"},
	{"key":"Color", "val": "16 zalm", "equals":"ORANGE"},
	{"key":"Color", "val": "18 oranje", "equals":"ORANGE"},
	{"key":"Color", "val": "19 donker grijs", "equals":"GREY"},
	{"key":"Color", "val": "20 zilver", "equals":"SILVER_COLORED"},
	{"key":"Color", "val": "21 mosterd geel", "equals":"YELLOW"},
	{"key":"Color", "val": "22 zilver", "equals":"SILVER_COLORED"},
	{"key":"Color", "val": "23 licht grijs", "equals":"GREY"},
	{"key":"Color", "val": "28 turquoise", "equals":"BLUE"},
	{"key":"Color", "val": "30 bi-color", "equals":""},
	{"key":"Color", "val": "33 bi-color", "equals":""},
	{"key":"Color", "val": "40 goud", "equals":"GOLD_COLORED"},
	{"key":"Color", "val": "42 zand", "equals":"BROWN"},
	{"key":"Color", "val": "44 goud", "equals":"GOLD_COLORED"},
	{"key":"Color", "val": "46 vanille", "equals":"YELLOW"},
	{"key":"Color", "val": "50 zilver", "equals":"SILVER_COLORED"},
	{"key":"Color", "val": "54 rose", "equals":"ROSE_GOLD_COLORED"},
	{"key":"Color", "val": "60 titanium", "equals":"SILVER_COLORED"},
	{"key":"Color", "val": "64 geel", "equals":"YELLOW"},
	{"key":"Color", "val": "65 cognac", "equals":"BROWN"},

	{"key":"strapMaterial", "val":"leer", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"staal", "equals":"STAINLESS_STEEL"},
	{"key":"strapMaterial", "val":"echt leer", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"echt kalfsleer", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"echt bison", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"echt buffel", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"echt hagedis", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"echt krokodil", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"echt alligator", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"echt krokodil xl", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"echt slang", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"echt stier", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"krokodil print", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"alligator print", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"lorica", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"lorica-carbon", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"terracare", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"lorica-terracare", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"gerecycled leer", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"rubber", "equals":"PLASTIC_SILICONE"},
	{"key":"strapMaterial", "val":"silicone", "equals":"PLASTIC_SILICONE"},
	{"key":"strapMaterial", "val":"plastic", "equals":"PLASTIC_SILICONE"},
	{"key":"strapMaterial", "val":"lak Leer", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"kwik vrij, Swiss made", "equals":""},
	{"key":"strapMaterial", "val":"carbon", "equals":"PLASTIC_SILICONE"},
	{"key":"strapMaterial", "val":"synth. leer", "equals":"PU_LEATHER"},
	{"key":"strapMaterial", "val":"lithium cel", "equals":""},
	{"key":"strapMaterial", "val":"nylon", "equals":"CANVAS"},
	{"key":"strapMaterial", "val":"hout", "equals":""},
	{"key":"strapMaterial", "val":"aluminium en hout", "equals":"METAL"},
	{"key":"strapMaterial", "val":"stof", "equals":"CANVAS"},
	{"key":"strapMaterial", "val":"hagedis print", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"staal", "equals":"STAINLESS_STEEL"},
	{"key":"strapMaterial", "val":"staal-goud", "equals":"STAINLESS_STEEL"},
	{"key":"strapMaterial", "val":"alkaline", "equals":""},
	{"key":"strapMaterial", "val":"aluminium", "equals":"METAL"},
	{"key":"strapMaterial", "val":"staal-pvd goud", "equals":"STAINLESS_STEEL"},
	{"key":"strapMaterial", "val":"aluminium-goud", "equals":"METAL"},
	{"key":"strapMaterial", "val":"set aanbieding", "equals":""},
	{"key":"strapMaterial", "val":"echt struisvogel", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"tejus print", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"titanium", "equals":"TITANIUM"},
	{"key":"strapMaterial", "val":"polyurethaan", "equals":"PU_LEATHER"},
	{"key":"strapMaterial", "val":"smart watch", "equals":""}
]

function init(productData) {
	
	const res = bbsParser(productData)

	let now = String(Math.floor(Date.now() / 1000))

   //Save the results to a JSON-file
	if(res && res.data && res.data.length > 0 && process.argv[2] === "--save") {
		
		fs.writeFile("./output/"+now+".json", JSON.stringify(res.data,null,2), function(err) {if(err) { throw new Error(err); }});
		console.log('result saved to /output')

		if(res.problems) {
			fs.writeFile("./output/"+now+"_problems.json", JSON.stringify(res.problems,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('problems saved to /output')
		}

		if(res.warnings) {
			fs.writeFile("./output/"+now+"_warnings.json", JSON.stringify(res.warnings,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('warnings saved to /output')

		}

		if(res.notifications) {
			fs.writeFile("./output/"+now+"_notifications.json", JSON.stringify(res.notifications,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('notifications saved to /output')

		}
	}
}

