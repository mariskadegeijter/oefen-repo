'use strict';
const fs = require('fs');

//# URL = "https://files.channable.com/PnSOXxfXvEZd58c_buT2rw==.xml"

var messages = []
var descriptions = []

function _removeAccents(strAccents) {
    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents =    "ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž";
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
        } else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');
    return strAccentsOut;
}

function _lower(str) {
	return str.toLowerCase()
}

function _chkVal(val) {
	if(Array.isArray(val)) {
		return Boolean(val.length > 0 && val[0] !== "")
	} else {
		return Boolean(val)
	}
}

function buildShortDescription(article,props,productNumber) {

	let params = {
		"material":"",
		"color":"",
		"additionalSpecs":[],
	}

	let result = ""
	

	// if(props['materiaal'].includes('Zilver')) {
	// 	params['material'] = "zilver"
	// } else if(props['materiaal'].includes('Staal')) {
	// 	params['material'] = "staal"
	// } else (props['materiaal'] === 'Leer') {
	// 		params['material'] = "leer"
	// 	} else {
	// 		// console.log(props['materiaal'])
	// 	}
	

	// if(!props['kleur'].includes('kleur')) {
	// 	params['color'] = String(props['kleur']+" van kleur").toLowerCase()
	// } else {
	// 	params['color'] = props['kleur'].toLowerCase()
	// }

	//Additional specs
	if(_chkVal(props[laenge])) {
		params.additionalSpecs.push(String('Lengte band: ' + props[laenge] + ' mm'))
	}

	if(_chkVal(props[breite])) {
		params.additionalSpecs.push(String('Breedte band: ' + props[breite] + ' mm'))
	}

	if(_chkVal(props[hoehe])) {
		params.additionalSpecs.push(String('Dikte band: ' + props[hoehe] + ' mm'))
	}

	if(_chkVal(props[gewicht])) {
		params.additionalSpecs.push(String('Gewicht: ' + props[gewicht] + ' gr'))
	}

	result = String("Dit is een horlogeband van het merk Pebro. De horlogeband is "+params['color']+" en gemaakt van "+params['material']+". ");

	
	if(params.additionalSpecs.length > 0) {
	 	result += "\nDe verdere specificaties van het horloge zijn als volgt:\n"
	 	result += params.additionalSpecs.join("\n")
	 }

	return result
}

//assigns the right values from the properties to the final article
function parseStrapProperties(article,props,currentProductNumber) {
	if(_chkVal(props['laenge'])) {
		article.strap.wristPermimeter = props[laenge]
	}

	if(_chkVal(props['breite'])) {
		article.strap.width = props[breite]
		article.size = props[breite]
		article.variants.size = props[breite]
	}

	if(_chkVal(props['leerfeld'])) {
		article.strap.material = getPrismaNoteProperty('strapMaterial',props['leerfeld'],"leerfeld",currentProductNumber,props['leerfeld'])
	}

	if(_chkVal(props[''])){
		article.strap.color = getPrismaNoteProperty('Color',props[''],"",currentProductNumber,props[''])
	}

	if(_chkVal(props['MATERIAAL'])) {
		if('MATERIAAL' == 'leer'){
			article.strap.model = 'STANDARD_MODEL'
			article.strap.clasp = "Gesp"
		} else if('MATERIAAL' == 'PU & PVC'){
			article.strap.model = 'STANDARD_MODEL'
			article.strap.clasp = "Gesp"
		} else if('MATERIAAL' == 'nylon'){
			article.strap.model = 'NATO'
			article.strap.clasp = 'Gesp'
		}
	}

	if(_chkVal(['gewicht']) {
		article.weight = props[gewicht]
	}
}


fs.readFile('./src/zinzi-api-output.json','utf8', function(errors,data) {
	if(data) {
		var jsonData = JSON.parse(data)

		if(process.argv.length < 3) {
			console.log("This is a dry run, the output is not saved.")
			console.log("Run this script as 'node zinzi.js --save' to save the output.")
		}

		if(jsonData) {
			init(jsonData);
		} else {
			throw new Error("Invalid JSON input")
		}
	} else {
		throw new Error("No JSON input")
	}
});

function zinziParser(srcData) {

	let productsFound = []
	let known_keys = []
	let known_types = []
	let sizes = []

	if(srcData.xml) {

		let articles = srcData.xml
		
		articles.forEach(function(art) {

			let article = {
				"images":[],
				"variants": [],
				"hasStock":true,
				"size":0,
				"weight":0,
				//"totalCaratWeight":0,
				// "gems":[
				// 	// {
				// 	// 	"quantity":0,
				// 	// 	"gemKind":"",
				// 	// 	"gemPurity":"",
				// 	// 	"gemColor":"",
				// 	// 	"gemcut":"",
				// 	// 	"caratWeight":0,
				// 	// 	"suggestedRetailPriceDiamond":0
				// 	// }
				// ],
				// "jewel":{
				// 	"material":"",
				// 	"color":"",
				// 	"type":"",
				// 	"height":0,
				// 	"width":0,
				// 	"depth":0,
				// 	"diameter":0,
				// 	"weight":0,
				// 	"chain":"",
				// 	"clasp":"",
				// 	"shape":"",
				// 	"gloss":false
				// },
				"strap":{
					"model":"",
					"width":0,
					"material":"",
					"color":"",
					"print":"",
					"studs":"",
					"clasp":"",
					"pattern":"",
					"wristPermimeter":""
				},
				// "watch":{
				// 	"type":"WRIST",
				// 	"movement":"",
				// 	"indication":"",
				// 	"hasSwissMovement":false,
				// 	"hasDateFunction":false,
				// 	"waterProofLevel":null,
				// 	"isNickelFree":false,
				// 	"isAntiAllergy":false,
				// 	"hasLightFunction":false,
				// 	"isSmartWatch":false,
				// 	"smartWatchFunctions":"",
				// 	"dial":{
				// 		"color":"",
				// 		"pattern":"",
				// 		"print":"",
				// 		"index":""
				// 	},
				// 	"case":{
				// 		"shape":"",
				// 		"size":0,
				// 		"depth":0,
				// 		"material":"",
				// 		"glassMaterial":"",
				// 		"color":""
				// 	}
				// },
				"category":"STRAP",
				"brand":{
					"name":"",
				},
				"suggestedRetailPrice":0,
				"suggestedRetailPriceVat":0,
				"male":true,
				"female":true,
				"kids":false,
				"nl":{
					"name":"",
					"shortDescription":"",
					"longDescription":""
				},
				"en":{
					"name":"",
					"shortDescription":"",
					"longDescription":""
				},
				"de":{
					"name":"",
					"shortDescription":"",
					"longDescription":""
				},
				"uploader": {
                "_id": "NECESSARY TO ADD",
                "name": "NECESSARY TO ADD"
                }
			}

			//extract properties and put in props object
			let props = {}
			for(let key in art) {
				if(!known_keys.includes(key)) {
					known_keys.push(key)
				}
				props[key] = String(art[key][0])
			}

			let currentProductNumber = _chkVal(props['nummer']) ? props['nummer'] : ""
			let currentProductNumberWithSD = _chkVal(props['nummer']) ? ['PEB',props['nummer']].join() : ""
			let currentEAN = _chkVal(props['ean']) ? props['ean'] : ""

			if(_chkVal(props['ean'])) {
				let currentEAN = props['ean']
			} else if(_chkVal(props[]))

			let variant = {
				"productNumberAuto":false,
				"size":0,
				"productNumber": currentProductNumberWithSD,
				"insertedProductNumber":currentProductNumber,
				"ean": currentEAN,
				"shopStratingDigits":"PEB"
				"brandStratingDigits": "PEB"
			}

			article.variants.push(variant)

			
			if(_chkVal(props['standardbild'])) {
				article.images.push({
					"src":props['standardbild'],
					"alt":props['id']
				})
				
			}

			article.nl.name = ['Pebro','horlogeband',props['nummer']].join(" ").toUpperCase()

			if(_chkVal(props['name_en'])) {
				article.en.name = props['name_en']
			}
			if(_chkVal(props['name_de'])) {
				article.de.name = props['name_de']
			}
			if(_chkVal(props['kurztext_en'])) {
				article.en.shortDescription = props['kurztext_en']
			}
			if(_chkVal(props['kurztext_de'])) {
				article.de.shortDescription = props['kurztext_de']
			}
			if(_chkVal(props['beschreibung_en'])) {
				article.en.shortDescription = props['beschreibung_en']
			}
			if(_chkVal(props['beschreibung_de'])) {
				article.de.shortDescription = props['beschreibung_de']
			}

			article.brand.name = "Pebro"

			if(_chkVal(props['verkaufspreise'])) { article.suggestedRetailPrice = parseFloat(props['verkaufspreise'])/1.21}
			if(_chkVal(props['verkaufspreise'])) { article.suggestedRetailPriceVAT = 21}

			article.nl.shortDescription = buildShortDescription(article,props,currentProductNumber)

			productsFound.push(article)
		});
	} else {
		throw new Error("No articles in source data")
	}

	console.warn('PLEASE CHECK IF VAT 21% SHOULD ALWAYS BE APPLIED!')

	//writing the sizes from the api to a file for development/testing purposes
	//fs.writeFile("./output/sizes.txt", sizes.join("\n"),{ flag: 'w' }, function(err) {if(err) { throw new Error(err); }});

	let problems = messages.filter(m => { return m.problem && m.problem !== '' });
	let warnings = messages.filter(m => { return m.warning && m.warning !== '' });
	let notifications = messages.filter(m => { return m.notification && m.notification !== '' });

	return {
		"data":productsFound,
		"problems":problems,
		"warnings":warnings,
		"notifications":notifications
	};
}

function getPrismaNoteProperty(type,val,srcProp,productNumber=null,material) {

	val = _removeAccents(val).toLowerCase()

	if(!["caseGlasstype","caseMaterial","caseShape","Color","dialIndex","dialPattern","gemKind ","jewelMaterial","jewelType","strapMaterial","strapModel","watchIndication","watchMovement","watchType"].includes(type)) {
		return null
	}

	let res = {
		"success":false,
		"data":"",
		"errors":[]
	}

	let targetProp = null

	switch(type) {

		case "caseGlasstype":
			targetProp = caseGlasstype
		break;
		case "caseMaterial":
			targetProp = caseMaterial	
		break;
		case "caseShape":
			targetProp = caseShape		
		break;
		case "Color":
			targetProp = Color
		break;
		case "dialIndex":
			targetProp = dialIndex
		break;
		case "dialPattern":
			targetProp = dialPattern
		break;
		case "gemKind ":
			targetProp = gemKind	
		break;
		case "jewelMaterial":
			targetProp = jewelMaterial	
		break;
		case "jewelType":
			targetProp = jewelType
		break;
		case "strapMaterial":
			targetProp = strapMaterial
		break;
		case "strapModel":
			targetProp = strapModel
		break;
		case "watchIndication":
			targetProp = watchIndication
		break;
		case "watchMovement":
			targetProp = watchMovement
		break;
		case "watchType":
			targetProp = watchType
		break;

		default:
			targetProp = null
		break;
	}

	if(_chkVal(targetProp)) {
		if(val in targetProp) { //there's an exact match
			res.success = true
			res.data = targetProp[val]
		} else { //there's no match, so let's try one of the synonyms
			
			synonyms.forEach(s => {
				if(type === s['key'] && val === s['val']) {
					res.success = true
					res.data = s['equals']
					messages.push({
						"product":(productNumber ? productNumber : "(no productnumber)"),
						"notification":String(type+" '"+val+"' - alias found: "+s['equals'])
					})
				}
			});

			if(!res.success) {
				overrides.forEach(s => {
					if(s['attr'] === type && s['of'] === srcProp && s['sku'] === productNumber) {
						res.data = s['equals']
						res.success = true
						messages.push({"product":(productNumber ? productNumber : "(no productnumber)"),"notification":String(type+" '"+val+"' overwritten by "+s['equals'])})
					}
				});

				if(!res.success) {

					
					if(!res.success) {
						console.log(String("Not found: "+productNumber+", "+srcProp+": "+val+" ("+type+")"))
					}
				}
			}
		}
	}

	if(res.success) {
		return res.data
	} else {
		messages.push({
			"product":(productNumber ? productNumber : "(no productnumber)"),
			"problem":String(type+" '"+val+"' not found for "+(srcProp ? srcProp : ""))
		})
		return;
	}
}

function init(productData) {
	
	const res = zinziParser(productData)

	let now = String(Math.floor(Date.now() / 1000))

   //Save the results to a JSON-file
	if(res && res.data && res.data.length > 0 && process.argv[2] === "--save") {
		
		fs.writeFile("./output/"+now+".json", JSON.stringify(res.data,null,2), function(err) {if(err) { throw new Error(err); }});
		console.log('result saved to /output')

		if(res.problems) {
			fs.writeFile("./output/"+now+"_problems.json", JSON.stringify(res.problems,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('problems saved to /output')
		}

		if(res.warnings) {
			fs.writeFile("./output/"+now+"_warnings.json", JSON.stringify(res.warnings,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('warnings saved to /output')

		}

		if(res.notifications) {
			fs.writeFile("./output/"+now+"_notifications.json", JSON.stringify(res.notifications,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('notifications saved to /output')

		}
	}
}

const synonyms = [
	{"key":"jewelType", "val":"ringen", "equals":"RING"},
	{"key":"jewelType", "val":"oorbellen", "equals":"HOOP_EARRINGS"},
	{"key":"jewelType", "val":"kettingen", "equals":"NECKLACE"},
	{"key":"jewelType", "val":"armbanden", "equals":"BRACELET"},
	{"key":"jewelType", "val":"hangers", "equals":"PENDANT"},
	{"key":"jewelType", "val":"sluitingen", "equals":"CHARM"},

	{"key":"jewelMaterial", "val":"zilver, geel verguld", "equals":"SILVER"},
	{"key":"jewelMaterial", "val":"zilver, rose verguld", "equals":"SILVER"},
	{"key":"jewelMaterial", "val":"zilver met leer", "equals":"SILVER"},
	{"key":"jewelMaterial", "val":"zilver met koord", "equals":"SILVER"},
	{"key":"jewelMaterial", "val":"14 karaat goud", "equals":"GOLD"},
	{"key":"jewelMaterial", "val":"staal", "equals":"STAINLESS_STEEL"},
	{"key":"jewelMaterial", "val":"staal geel vergulde kleur", "equals":"STAINLESS_STEEL"},
	{"key":"jewelMaterial", "val":"staal rose vergulde kleur", "equals":"STAINLESS_STEEL"},

	{"key":"Color", "val":"14 karaat goud", "equals":"GOLD"},
	{"key":"Color", "val":"champagne", "equals":"CREME"},	
	{"key":"Color", "val":"turquoise", "equals":"BLUE"},
	{"key":"Color", "val":"rosegoudkleurig", "equals":"ROSE_GOLD_COLORED"},
	{"key":"Color", "val":"geelgoudkleurig", "equals":"GOLD_COLORED"},

	{"key":"Color", "val":"zilver", "equals":"SILVER_COLORED"},
	{"key":"Color", "val":"geelverg", "equals":"GOLD_COLORED"},
	{"key":"Color", "val":"roseverg", "equals":"ROSE_GOLD_COLORED"},

	{"key":"strapMaterial", "val":"leer", "equals":"REAL_LEATHER"},
	{"key":"strapMaterial", "val":"staal", "equals":"STAINLESS_STEEL"}
]

// const overrides = [
// 	{"sku":"RETBAND13", "attr":"Color", "of":"kleur", "equals":"GOLD_COLORED"},
// 	{"sku":"RETBAND2", "attr":"Color", "of":"kleur", "equals":"ROSE_GOLD_COLORED"},
// 	{"sku":"RETBAND28", "attr":"Color", "of":"kleur", "equals":"YELLOW"}
// ]

// const gemKind = {
// 	"agaat" : "AGATE",
// 	"amazone" : "AMAZONE",
// 	"amazoniet" : "AMAZONITE",
// 	"barnsteen" : "AMBER",
// 	"amethyst" : "AMETHYST",
// 	"aquamarijn" : "AQUAMARINE",
// 	"aventurijn" : "AVENTURINE",
// 	"beryl" : "BERYLL",
// 	"bloedkoraal" : "BLOODCORAL",
// 	"calciet" : "CALCITE",
// 	"cameo" : "CAMEO",
// 	"carneool" : "CARNELEAN",
// 	"cateye" : "CATEYE",
// 	"keramiek" : "CERAMICS",
// 	"chalcedoon" : "CHALCEDONY",
// 	"citrien" : "CITRINE",
// 	"korund" : "CORUNDUM",
// 	"kristal" : "CRYSTAL",
// 	"diamant" : "DIAMOND",
// 	"smaragd" : "EMERALD",
// 	"glazuur" : "ENAMEL",
// 	"epidote" : "EPIDOTE",
// 	"firepost" : "FIREPOST",
// 	"fluoriet" : "FLUORITE",
// 	"granaat" : "GARNET",
// 	"glas" : "GLASS",
// 	"heliotroop" : "HELIOTROPE",
// 	"hematiet" : "HEMATITE",
// 	"iriskwarts" : "IRISQUARTZ",
// 	"jade" : "JADE",
// 	"jargon" : "JARGON",
// 	"jasper" : "JASPER",
// 	"labradoriet" : "LABRADORITE",
// 	"lapis" : "LAPIS",
// 	"lazuli" : "LAZULI",
// 	"lazuriet" : "LAZURITE",
// 	"citroenkwarts" : "LEMONQUARTZ",
// 	"madeiracitrine" : "MADEIRACITRINE",
// 	"malachiet" : "MALACHITE",
// 	"maansteen" : "MOONSTONE",
// 	"muranoglass" : "MURANOGLASS",
// 	"natuursteen" : "NATURALSTONE",
// 	"nefriet" : "NEPHRITE",
// 	"onyx" : "ONYX",
// 	"opaal" : "OPAL",
// 	"parel" : "PEARL",
// 	"parel akoya" : "PEARLAKOYA",
// 	"parel cultive" : "PEARLCULTIVE",
// 	"parel zuidzee" : "PEARLSOUTHSEA",
// 	"parel zoetwater" : "PEARLSWEETWATER",
// 	"parel synthetisch" : "PEARLSYNTHETICAL",
// 	"parel tahiti" : "PEARLTAHITI",
// 	"peridoot" : "PERIDOTE",
// 	"fosforiet" : "PHOSPHORITE",
// 	"quartz" : "QUARTZ",
// 	"strass" : "RHINESTONE",
// 	"steenkristal" : "ROCKCRYSTAL",
// 	"rozenkwarts" : "ROSEQUARTZ",
// 	"robijn" : "RUBY",
// 	"saffier" : "SAPPHIRE",
// 	"leisteen" : "SLATE",
// 	"rookkwarts" : "SMOKYQUARTZ",
// 	"soladiet" : "SODALITE",
// 	"spinel" : "SPINEL",
// 	"laagsteen" : "STRATUMSTONE",
// 	"swarovski kristal" : "SWAROVSKICRYSTAL",
// 	"synthetisch" : "SYNTHETICSTONE",
// 	"tijgeroog" : "TIGEREYE",
// 	"topaas" : "TOPAZ",
// 	"toermalijn" : "TOURMALINE",
// 	"turquoise" : "TURQUOISE",
// 	"uvaroviet" : "UVAROVITE",
// 	"wildvinite" : "WILDVINITE",
// 	"xylopile" : "XYLOPILE",
// 	"zirkonia" : "ZIRCONIA"
// }

// const jewelType = {
// 	"enkelbandje" : "ANKLE_BRACELET",
// 	"armband" : "BRACELET",
// 	"broche" : "BROOCH",
// 	"?" : "CHARM",
// 	"choker" : "CHOKER",
// 	"combinatiering" : "COMBINATION_RING",
// 	"oorringen" : "CREOLE_EARRINGS",
// 	"manchetknopen" : "CUFFLINK",
// 	"oorhangers" : "HOOP_EARRINGS",
// 	"collier/ketting" : "NECKLACE",
// 	"hanger/bedel/charm" : "PENDANT",
// 	"ring" : "RING",
// 	"ring met edelsteen" : "RING_WITH_GEM",
// 	"ring met parel" : "RING_WITH_PEARL",
// 	"set" : "SET",
// 	"bangle armband" : "SLAVE_BRACELET",
// 	"oorstekers" : "STUD_EARRINGS",
// 	"tennisarmband" : "TENNIS_BRACELET",
// 	"tennis ketting" : "TENNIS_NECKLACE"
// }

// const jewelMaterial = {
// 	"goud" : "GOLD",
// 	"zilver" : "SILVER",
// 	"hout" : "WOOD",
// 	"canvas / nylon" : "CANVAS",
// 	"lederen bovenkant" : "UPPER_LEATHER",
// 	"pu leder" : "PU_LEATHER",
// 	"metaal" : "METAL",
// 	"aluminium" : "ALUMINIUM",
// 	"keramiek" : "CERAMICS",
// 	"edelstaal" : "STAINLESS_STEEL"
// }

const Color = {
	"beige":"BEIGE",
	"zwart":"BLACK",
	"blauw":"BLUE",
	"brons":"BRASS",
	"bruin":"BROWN",
	"creme":"CREME",
	"goudkleurig":"GOLD_COLORED",
	"goud zilver kleurig":"GOLD_SILVER_COLORED",
	"groen":"GREEN",
	"grijs":"GREY",
	"parelmoer":"MOTHER_OF_PEARL",
	"parelmoer gekleurd":"MOTHER_OF_PEARL_COLOURED",
	"oranje":"ORANGE",
	"roze":"PINK",
	"paars":"PURPLE",
	"rood":"RED",
	"roségoudkleurig":"ROSE_GOLD_COLORED",
	"rosé zilver gekleurd":"ROSE_SILVER_COLORED",
	"zilverkleurig":"SILVER_COLORED",
	"taupe":"TAUPE",
	"wit":"WHITE",
	"geel":"YELLOW"
	"schwarz": "BLACK"
	"dunkelbraun": "BROWN"
	"weiß": "WHITE"
	"braun": "BRAUN"
	"grau": "GREY"
	"dunkelblau": "BLUE"
	"blau": "BLUE"
	"dunkelrot": "RED"
	"rot": "RED"
	"grün": "GREEN"
	"sand": "BROWN"
	"crème": "CREME"
	"olivbraun": "BRAUN"
	"cognac": "BRAUN"
	"bordeaux rot": "RED"
	"beige": "BEIGE"
	"hellblau": "BLUE"
	"gelb": "YELLOW"
	"rosa": "ROSE_GOLD_COLORED"
	"himbeerrot": "RED"
	"schwarz / rot": "BLACK"
	"schlammbraun":"BRAUN"
	"natur":" "
	"türkis": "BLUE"
	"rubinrot": "RED"
	"magenta": "PURPLE"
	"orange": "ORANGE"
	"porzellan": " "
	"senf": " "
	"petrolblau": "BLUE"
	"kastanie": "BROWN"
	"violett": "PURPLE"
	"weinrot": "RED"
	"dunkelgrün": "GREEN"
	"haselnuss": "BROWN"
	"terracotta": " "
	"jeans": "BLUE"
	"taupe": "TAUPE"
	"hellbraun": "BRAUN"
	"anthrazit": "GREY"
	"silber": "SILVER_COLORED"
	"gold": "GOLD_COLORED"
	"hellgrau": "GREY"
	"olivgrün": "GREEN"
	"olivgrün / schwarz": "GREEN"
	"schwarz / weiß": "BLACK"
	"Edelstahl": "SILVER_COLORED"
	"PVD gold": "GOLD_COLORED"
	"PVD rosé": "ROSE_GOLD_COLORED"
	"PVD anthrazit": "BLACK"
	"Bicolor PVD gold":"GOLD_SILVER_COLORED"
	"Bicolor PVD rosé": "ROSE_SILVER_COLORED"
	"vergoldet": "GOLD_COLORED"
}

const strapMaterial = {
	"aluminium" : "ALUMINIUM",
	"echt leer" : "REAL_LEATHER",
	"lederen bovenkant" : "UPPER_LEATHER",
	"metaal" : "METAL",
	"kunststof / rubber" : "PLASTIC_SILICONE",
	"edelstaal" : "STAINLESS_STEEL",
	"titanium" : "TITANIUM",
	"keramiek" : "CERAMICS",
	"pu leder" : "PU_LEATHER",
	"canvas / nylon" : "CANVAS",
	"echt goud" : "REAL_GOLD"
}

const strapModel = {
	"rekband" : "EXPANDABLE_STRETCH_STRAP",
	"schakelketting zakhorloge" : "SELECT_BAND_FOR_POCKET_WATCH",
	"schakelband" : "SELECT_BAND",
	"milanees / mesh" : "MILANESE_MESH",
	"nato" : "NATO",
	"standaard model" : "STANDARD_MODEL"
}

const caseMaterial = {
	"titanium" : "TITANIUM",
	"edelstaal" : "STAINLESS_STEEL",
	"aluminium" : "ALUMINIUM",
	"metaal" : "METAL",
	"kunststof / rubber" : "PLASTIC_SILICONE",
	"hout" : "WOOD",
	"keramiek" : "CERAMICS"
}


const caseGlasstype = {
	"saffier" : "SAPPHIRE",
	"saffier gecoat" : "SAPPHIRE_COATED",
	"kristal" : "CRYSTAL",
	"mineraal" : "MINERAL",
	"kunststof / rubber" : "SYNTHETIC_PLASTIC",
}

const caseShape = {
	"ovaal" : "OVAL",
	"rechthoek" : "RECTANGLE",
	"rond" : "ROUND",
	"andere vorm" : "OTHER_SHAPE",
	"vierkant" : "SQUARE"
}

const dialIndex = {
	"cijfers (arabisch)" : "ARABIC",
	"puntjes" : "DOTS",
	"romeinse cijfers" : "ROMAN",
	"streepjes" : "STRIPES",
	"geen" : "NONE",
	"sierdiamanten (zirkonia / synthetisch)" : "ORNAMENTAL_DIAMONDS",
	"echte diamanten" : "REAL_DIAMONDS"
}

const dialPattern = {
    "puntjes":"DOTS",
    "streepjes":"STRIPES"
}

const watchIndication = {
	"analoog" : "ANALOG",
	"analoog en digitaal" : "ANALOG_DIGITAL",
	"chrono / multi" : "CHRONO_MULTI",
	"digitaal" : "DIGITAL",
}

const watchMovement = {
	"quartz" : "QUARTZ",
	"solar (zonne energie)" : "SOLAR",
	"gps solar" : "GPS_SOLAR",
	"kinetisch" : "KINETIC",
	"automatisch" : "AUTOMATIC",
	"handopwind" : "HAND_WINDING",
}

const watchType = {
	"smartwatch":"SMARTWATCH",
	"polshorloge":"WRIST",
	"verpleegsterhorloge":"NURSE",
	"zakhorloge":"POCKET"
}

const colorSpellingFix = {
	"Rood":"rode",
	"Zilverkleurig":"zilver",
	"Turquoise":"turquoise",
	"Oranje":"oranje",
	"Zwart":"zwarte",
	"Paars":"paarse",
	"Wit":"witte",
	"Blauw":"blauwe",
	"Roze":"roze",
	"Bruin":"bruine",
	"Grijs":"grijze",
	"Groen":"groene",
	"Champagne":"crèmekleurige"
}