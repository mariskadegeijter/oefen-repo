import json
import os

data = None

json_file = open(os.path.join(os.path.dirname(__file__), "output/output.json"),'r')

data = json.load(json_file)
json_file.close()

length = len(data)
counter = 1
for product in data:
	print(str(counter)+"/"+str(length))
	for k in list(product['specs'].keys()):
		if product['specs'][k] == None:
			del product['specs'][k]
	counter += 1

print("Saving to file..")

json_data = json.dumps(data, indent=2)
with open('sanitized.json','w') as f:
	f.write(json_data)
print("Saved!")

