import requests
import json
import datetime
import time
import os
import sys

conn_details = {
	'email':'prismanote@weiszgroup.com',
	'passw':'z0DADheXEUGEghPU',
	'items_per_page' :500,
}

def get_page(conn_details, page_nr):
	res = requests.get(url='https://api.weisz.improvit-staging.nl/api/v1/feed/items', params={'per_page':conn_details['items_per_page'], 'page':page_nr}, headers = {'Authorization':str("Bearer "+conn_details['access_token'])})
	if res.status_code == 200:
		return json.loads(res.text)
	else:
		return None

if not os.path.isdir('output'):
	print('No directory "output" found. Create it before running this script.')
	input('Press Enter to close this script')
	sys.exit()

if len(sys.argv) > 1:
	data_result = []

	conn_details['access_token'] = sys.argv[1]
	print(str("Authenticated with token "+conn_details['access_token']))

	first_page = get_page(conn_details, 1)

	if first_page:
		total_pages = first_page['last_page']
		for item in first_page['data']:
			data_result.append(item)

		for i in range(2,total_pages+1):
			print('Getting page '+str(i)+'/'+str(total_pages))
			curr_page = get_page(conn_details, i)
			for item in curr_page['data']:
				data_result.append(item)
			time.sleep(1)

		with open(os.path.join(os.path.dirname(__file__), "output/output.json"), "w") as f1:
			f1.write(json.dumps(data_result,indent=2))

		print('Finished!')
else:
	raise Exception('Could not authenticate. Check README.md for instructions')
# if items_res.status_code == 200:
# 	
# 	print(items_obj)


# now = str(datetime.datetime.now().timestamp()).split(".")[0]
# auth = requests.post("https://www.bbs-shop.com/api/auth/get_tokens?username=BBS@API&password=productfeed&db=BBS")

# if auth.status_code == 200:
# 	res = json.loads(auth.text)
# 	if 'access_token' in res['data']:
# 		token = res['data']['access_token']
# 		products = requests.get(url="https://www.bbs-shop.com/api/product.template", headers = {"access-token":token, "email":"data@prismanote.com"})
# 		if products.status_code == 200:
# 			with open(str(now)+".json", "w") as f1:
# 				f1.write(products.text)
# 			print('Saved to file!')
# 		else:
# 			print('invalid product request..')
# 	else:
# 		print('no access-token in auth response')
# else:
# 	print('invalid auth request..')