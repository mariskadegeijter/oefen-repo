# Weisz API demo

## Usage
1. Open Powershell or Command Prompt and run `python auth.py`.
1. This will generate a token and store it in `key.txt`. Open that file and copy the key
1. Call `download.py` with the key you just copied, like this: `python download.py "37|REsK74SEEeW4ku0tN4Icd9VorEgAKAg8qwS2SEqt"`
1. The output of `download.py` will be stored in `output/output.json`
1. Optional (but recommended): `python sanitize.py`: This will get the `output/output.json` and remove all attributes with value:null from the products