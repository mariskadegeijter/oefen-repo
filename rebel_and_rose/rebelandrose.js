'use strict';
const fs = require('fs');

var messages = []
var descriptions = []

function _removeAccents(strAccents) {
	if(isNaN(strAccents)) {
		strAccents = strAccents.split('');
		var strAccentsOut = new Array();
		var strAccentsLen = strAccents.length;
		var accents =    "ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž";
		var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
		for (var y = 0; y < strAccentsLen; y++) {
			if (accents.indexOf(strAccents[y]) != -1) {
				strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
			} else
				strAccentsOut[y] = strAccents[y];
		}
		strAccentsOut = strAccentsOut.join('');
		return strAccentsOut;
	} else {
		return false
	}
}

function _lower(str) {
	return str.toLowerCase()
}

function _chkVal(val) {
	if(Array.isArray(val)) {
		return Boolean(val.length > 0 && val[0] !== "")
	} else {
		return Boolean(val)
	}
}

function _chkValFalse(val) {
	if(Array.isArray(val)) {
		return Boolean(val.length > 0 && val[0] !== "" && val[0] !== false)
	} else {
		return Boolean(val)
	}
}

function _convertStripe(str) {
	return String(str).replace("_","-")
}

function _convertMultipleStripes(str){
	return _convertStripe(_convertStripe(_convertStripe(_convertStripe(_convertStripe(str)))))
}

function _replaceMMandComma(str){
	return String(str).replace("mm","").replace(",",".")
}

function _replaceSzComma(str){
	return String(str).replace("sz.","").replace(",",".")
}

function _isDefined(arr) {
	return arr !== undefined ? arr : false
}

function containsNumber(str) {
	return /\d/.test(str);
}

function noWhitespace(str) {
	return String(str).replace(/\s/g, '')
} 

//assigns the right values from the properties to the final article
function buildShortDescription(article,art,currentProductNumber){
	let result = ""
	let materiaalzin = ""
	let sieraadtype = ""

	if(_chkValFalse(art['product_type'])){
		if(art['product_type'].toLowerCase() === "ring"){
			sieraadtype = "Ring"
		} else if(art['product_type'].toLowerCase() === "pendant"){
			sieraadtype = "Hanger"
		} else if(art['product_type'].toLowerCase() === "necklace"){
			sieraadtype = "Collier"
		} else if(art['product_type'].toLowerCase() === "anklet"){
			sieraadtype = "Enkelbandje"
		} else if(art['product_type'].toLowerCase() === "bracelet"){
			sieraadtype = "Armband"
		} else if(art['product_type'].toLowerCase() === "accessoires"){
			if(art['article_description'].toLowerCase().includes('tattoo')){
				sieraadtype = "Tattoo"
			} else if(art['article_description'].toLowerCase().includes('keychain')){
				sieraadtype = "Sleutelhanger"
			} else if(art['article_description'].toLowerCase().includes('bandana')){
				sieraadtype = "Bandana"
			} else if(art['article_description'].toLowerCase().includes('shopping bag')){
				sieraadtype = "Tas"
			} else if(art['article_description'].toLowerCase().includes('gift wrapping')){
				sieraadtype = "Cadeauverpakking"
			} else if(art['article_description'].toLowerCase().includes('travel case')){
				sieraadtype = "Travel case"
			} else if(art['article_description'].toLowerCase().includes('collectors box')){
				sieraadtype = "Collectors box"
			}
		}
	}

	if(_chkValFalse(art['material'])){
		if(art['material'] === "sterling silver 925"){
			materiaalzin = ", gemaakt van sterling zilver (925)."
		} else if(art['material'].toLowerCase() === "paper"){
			materiaalzin = ", gemaakt van papier."
		} else if(art['material'].toLowerCase() === "leather"){
			materiaalzin = ", gemaakt van leer."
		} else if(art['material'].toLowerCase() === "cotton"){
			materiaalzin = ", gemaakt van katoen."
		} else if(art['material'].toLowerCase() === "all stainless steel"){
			materiaalzin = ", gemaakt van edelstaal."
		} else if(art['material'].toLowerCase() === "gemstones"){
			materiaalzin = String(". De armband heeft een hoogwaardige elastische stretch koord, met "+art['width_mm'].toLowerCase()+" van "+art['stone_nl']+".")
		} else(materiaalzin = ".")
	} else(materiaalzin = ".")

	article.nl.name = "REBEL & ROSE " + sieraadtype + " " + currentProductNumber
	result = String(sieraadtype+" van het merk Rebel & Rose"+materiaalzin)
	
	return result
}

function buildName(article,art,currentProductNumber){
	let geslacht = ""
	let geslachtEN = ""
	let geslachtDE = ""
	let sieraadtype = ""
	let sieraadtypeEN = ""
	let sieraadtypeDE = ""

	if(_chkValFalse(art['product_type'])){
		if(art['product_type'].toLowerCase() === "ring"){
			sieraadtype = "Ring"
			sieraadtypeEN = "Ring"
			sieraadtypeDE = "Ring"
		} else if(art['product_type'].toLowerCase() === "pendant"){
			sieraadtype = "Hanger"
			sieraadtypeEN = "Pendant"
			sieraadtypeDE = "Anhänger"
		} else if(art['product_type'].toLowerCase() === "necklace"){
			sieraadtype = "Collier"
			sieraadtypeEN = "necklace"
			sieraadtypeDE = "halskette"
		} else if(art['product_type'].toLowerCase() === "anklet"){
			sieraadtype = "Enkelbandje"
			sieraadtypeEN = "anklet"
			sieraadtypeDE = "fusskettchen"
		} else if(art['product_type'].toLowerCase() === "bracelet"){
			sieraadtype = "Armband"
			sieraadtypeEN = "bracelet"
			sieraadtypeDE = "armband"
		} else if(art['product_type'].toLowerCase() === "accessoires"){
			if(art['article_description'].toLowerCase().includes('tattoo')){
				sieraadtype = "Tattoo"
				sieraadtypeEN = "tattoo"
				sieraadtypeDE = "tattoo"
			} else if(art['article_description'].toLowerCase().includes('keychain')){
				sieraadtype = "Sleutelhanger"
				sieraadtypeEN = "keychain"
				sieraadtypeDE = "schlusselanhanger"
			} else if(art['article_description'].toLowerCase().includes('bandana')){
				sieraadtype = "Bandana"
				sieraadtypeEN = "bandana"
				sieraadtypeDE = "bandana"
			} else if(art['article_description'].toLowerCase().includes('shopping bag')){
				sieraadtype = "Tas"
				sieraadtypeEN = "shopping bag"
				sieraadtypeDE = "tasche"
			} else if(art['article_description'].toLowerCase().includes('gift wrapping')){
				sieraadtype = "Cadeauverpakking"
				sieraadtypeEN = "gift wrapping"
				sieraadtypeDE = "geschenkverpackung"
			} else if(art['article_description'].toLowerCase().includes('travel case')){
				sieraadtype = "Travel case"
				sieraadtypeEN = "Travel case"
				sieraadtypeDE = "Travel case"
			} else if(art['article_description'].toLowerCase().includes('collectors box')){
				sieraadtype = "Collectors box"
				sieraadtypeEN = "collectors box"
				sieraadtypeDE = "collectors box"
			}
		}
	}

	if(_chkValFalse(art['gender'])){
		if(art['gender'].toLowerCase() === "ladies"){
			geslacht = "dames"
			geslachtEN = "ladies"
			geslachtDE = "dammen"
		} else if(art['gender'].toLowerCase() === "gents"){
			geslacht = "heren"
			geslachtEN = "gents"
			geslachtDE = "herren"
		} 
	}

	article.nl.name = ("rebel & rose "+sieraadtype+" "+geslacht+" "+ currentProductNumber).toUpperCase().replace("  "," ")
	article.en.name = ("rebel & rose "+sieraadtypeEN+" "+geslachtEN+" "+ currentProductNumber).toUpperCase().replace("  "," ")
	article.de.name = ("rebel & rose "+sieraadtypeDE+" "+geslachtDE+" "+ currentProductNumber).toUpperCase().replace("  "," ")

	return article
}

function parseJewelProperties(article,art,currentProductNumber){
    article.category = "JEWEL" 
    
    if(_chkValFalse(art['material'])) {
		article.jewel.material = getPrismaNoteProperty('jewelMaterial', art['material'], "material", currentProductNumber, art['material'])
	}

    if(_chkValFalse(art['colors'][0])){
        article.jewel.color = getPrismaNoteProperty('Color', art['colors'][0], 'Color', currentProductNumber, art['colors'])
    }

	if(_chkValFalse(art['product_type'])){
		article.jewel.type = getPrismaNoteProperty('jewelType', art['product_type'], 'jewelType', currentProductNumber, art['product_type'])
	}

	if(article.jewel.type === "RING"){
		if(_chkValFalse(art['size_cm'])){
		article.jewel.diameter = _replaceSzComma(art['size_cm'])
		}
		if(_chkValFalse(art['size_w4u'])){
			article.size = art['size_w4u']
		}
	} else if(_chkValFalse(art['size_cm'])){
		article.size = art['size_cm']*10
	}

	if(_chkValFalse(art['width_specs'])){
		article.jewel.width = _replaceMMandComma(art['width_specs'])
	}

	if(_chkValFalse(art['total_weight'])){
		article.jewel.weight = art['total_weight']
	}
	
	return article

}

function parseGemKind(article,art,currentProductNumber){
	let gemKinds = art['stone_en'].split(",")
	if(_chkValFalse(gemKinds[0])){
		let gemKind1 = getPrismaNoteProperty('gemKind', gemKinds[0], 'gemKind',currentProductNumber, gemKinds[0])
		if(_chkValFalse(gemKind1)){
			article.gems.push({
				quantity: 0,
				gemKind: gemKind1,
				gemPurity: '',
				gemColor: '',
				gemcut: '',
				caratWeight: 0,
				suggestedRetailPriceDiamond: 0
			})
		} 
	}
	if(_chkValFalse(gemKinds[1])){
		let gemKind2 = getPrismaNoteProperty('gemKind', gemKinds[1], 'gemKind',currentProductNumber, gemKinds[1])
		if(_chkValFalse(gemKind2)){
			article.gems.push({
				quantity: 0,
				gemKind: gemKind2,
				gemPurity: '',
				gemColor: '',
				gemcut: '',
				caratWeight: 0,
				suggestedRetailPriceDiamond: 0
			})
		}
	}	
	if(_chkValFalse(gemKinds[2])){
		let gemKind3 = getPrismaNoteProperty('gemKind', gemKinds[2], 'gemKind',currentProductNumber, gemKinds[2])
		if(_chkValFalse(gemKind3)){
			article.gems.push({
				quantity: 0,
				gemKind: gemKind3,
				gemPurity: '',
				gemColor: '',
				gemcut: '',
				caratWeight: 0,
				suggestedRetailPriceDiamond: 0
			})
		}
	}
	return article
}

fs.readFile('./src/rr_api_output.json','utf8', function(errors,data) {	
    if(data) {
        var jsonData = JSON.parse(data)
    
        console.log(errors)
        //console.log(data)
    
        if(process.argv.length < 3) {
            console.log("This is a dry run, the output is not saved.")
            console.log("Run this script as 'node brand.js --save' to save the output.")
        }
        if(jsonData) {
            init(jsonData);
        } else {
            throw new Error("Invalid JSON input")
        }
    } else {
        throw new Error("No JSON input")
    }
});

function rrParser(srcData) {
	let productsFound = []
	let known_keys = []
	let known_types = []
	let sizes = []

	if(srcData) {

		let articles = srcData
		
		articles.forEach(function(art) {
			let article = {
				"images":[],
				"variants": [],
				"hasStock":true,
				"size":0,
				"weight":0,
				"totalCaratWeight":0,
				"gems":[],
				"jewel":{
					"material":"",
					"color":"",
					"type":"",
					"height":0,
					"width":0,
					"depth":0,
					"diameter":0,
					"weight":0,
					"chain":"",
					"clasp":"",
					"shape":"",
					"gloss":false
				},
				"strap":{
					"model":"",
					"width":0,
					"material":"",
					"color":"",
					"print":"",
					"studs":"",
					"clasp":"",
					"pattern":"",
					"wristPermimeter":""
				},
				"watch":{
					"type":"",
					"movement":"",
					"indication":"",
					"hasSwissMovement":false,
					"hasDateFunction":false,
					"waterProofLevel":null,
					"isNickelFree":false,
					"isAntiAllergy":false,
					"hasLightFunction":false,
					"isSmartWatch":false,
					"smartWatchFunctions":"",
					"dial":{
						"color":"",
						"pattern":"",
						"print":"",
						"index":""
					},
					"case":{
						"shape":"",
						"size":0,
						"depth":0,
						"material":"",
						"glassMaterial":"",
						"color":""
					},
					"strap":{
						"model":"",
						"width":0,
						"material":"",
						"color":"",
						"print":"",
						"studs":"",
						"clasp":"",
						"pattern":"",
						"wristPermimeter":""
					},
				},
				"category":"",
				"brand":{
					"name":"Rebel & Rose",
				},
				"suggestedRetailPrice":0,
				"suggestedRetailPriceVat":0,
				"male":true,
				"female":true,
				"kids":false,
				"nl":{
					"name":"",
					"shortDescription":"",
					"longDescription":""
				},
				"en":{
					"name":"",
					"shortDescription":"",
					"longDescription":""
				},
				"de":{
					"name":"",
					"shortDescription":"",
					"longDescription":""
				},
				"uploader": {
                "_id": "",
                "name": "REBEL & ROSE"
                }
			}

            //set category
			if(art['product_group'] === 'Jewelry'){
				article.category = "JEWEL"
			} else(article.category = "OTHER")

			//set variant
            let currentProductNumber = art['article_number']
            let currentEAN = art['ean']
			let size = ""

			if(art['product_type'].toLowerCase() === "ring"){
				if(_chkValFalse(art['size_w4u'])){
					size = art['size_w4u']
				}
			} else if(article.category === "OTHER"){
			} else if(currentProductNumber === "RR-NL012-S-52"){
				console.log(currentProductNumber,'My size is 520')
				size = 520
			} else if(_chkValFalse(art['size_cm'])){
				size = art['size_cm']*10
			}
			
			let variant = {
				"productNumberAuto":false,
				"size": size,
				"productNumber":currentProductNumber,
				"insertedProductNumber":currentProductNumber,
				"ean":currentEAN,
				"shopStratingDigits":""
			}

			article.variants.push(variant)

            //set images
			if(_chkValFalse(art.images[0])) {
				article.images.push({
				src: art.images[0] ,
				alt: _chkVal(art['article_description']) ? (art['article_description']+" foto 1") : currentProductNumber
				})
			} else {
				if(!_chkVal(art.images[0])) { messages.push({"product": currentProductNumber, "problem":"has no image url"}) }
			}
            if(_chkValFalse(art.images[1])) {
				article.images.push({
				src: art.images[1] ,
				alt: _chkVal(art['article_description']) ? (art['article_description']+" foto 2") : currentProductNumber
				})
			} 
            if(_chkValFalse(art.images[2])) {
				article.images.push({
				src: art.images[2] ,
				alt: _chkVal(art['article_description']) ? (art['article_description']+" foto 3") : currentProductNumber
				})
			}
            if(_chkValFalse(art.images[3])) {
				article.images.push({
				src: art.images[3] ,
				alt: _chkVal(art['article_description']) ? (art['article_description']+"foto 4") : currentProductNumber
				})
			}

			//set gender
			article.male = true
			article.female = true
			article.kids = false
			if(_chkValFalse(art['gender'])){
				if(art['gender'].toLowerCase() === "gents"){
					article.female = false
				} else if(art['gender'].toLowerCase() === "ladies"){
					article.male = false
				}
			} 
			
			//set brandname		
			article.brand.name = "Rebel & Rose"

			//set price
			if(_chkVal(art['consumer_price_from'])) { 
				article.suggestedRetailPrice = parseFloat(art['consumer_price_from'])/1.21
			} else if(_chkVal(art['consumer_price_for'])){
				article.suggestedRetailPrice = parseFloat(art['consumer_price_for'])/1.21
			}
			article.suggestedRetailPriceVat = 21

			//set description
			if(_chkValFalse(art['bol_description'])){
				article.nl.shortDescription = art['bol_description']	
			} else(
				article.nl.shortDescription = buildShortDescription(article,art,currentProductNumber)
				)
			if(_chkValFalse(art['article_description'])){
				article.en.shortDescription = art['article_description']
			}

			//set name
			article = buildName(article,art,currentProductNumber)

			//set gemkind	
			if(_chkValFalse(art['stone_en'])){
			article = parseGemKind(article, art, currentProductNumber)
			}

			//set properties
			if(article.category === 'JEWEL'){
				delete article.watch
				delete article.strap
				article = parseJewelProperties(article,art,currentProductNumber)
			} else {
				delete article.jewel
				delete article.strap
				delete article.watch
			}

			productsFound.push(article)
			//console.log(article)

           
        });
	} else {
		throw new Error("No articles in source data")
    }

	console.log('finished')
	console.warn('PLEASE CHECK IF VAT 21% SHOULD ALWAYS BE APPLIED!')
	console.warn('PLEASE ADD VARIANTCOLLECTION')

	let problems = messages.filter(m => { return m.problem && m.problem !== '' });
	let warnings = messages.filter(m => { return m.warning && m.warning !== '' });
	let notifications = messages.filter(m => { return m.notification && m.notification !== '' });

	console.log("Problems: ",problems.length)
    console.log("Warnings: ",warnings.length)
    console.log("Notifications: ",notifications.length)

	return {
		"data":productsFound,
		"problems":problems,
		"warnings":warnings,
		"notifications":notifications
	};
}

function getPrismaNoteProperty(type,val,srcProp,productNumber=null,material) {

	val = _removeAccents(val)

	val = (typeof val === 'string') ? val.toLowerCase() : val

	val = (typeof val === 'string') ? val.trim() : val


	if(![
		"caseGlasstype",
		"caseMaterial",
		"caseShape",
		"Color",
		"dialIndex",
		"dialPattern",
		"gemKind",
		"jewelMaterial",
		"jewelType",
		"strapMaterial",
		"strapModel",
		"watchIndication",
		"watchMovement",
		"watchType"
		].includes(type)) {
		return null
	}

	let res = {
		"success":false,
		"data":"",
		"errors":[]
	}

	let targetProp = null

	switch(type) {

		case "caseGlasstype":
			targetProp = caseGlasstype
		break;
		case "caseMaterial":
			targetProp = caseMaterial	
		break;
		case "caseShape":
			targetProp = caseShape		
		break;
		case "Color":
			targetProp = Color
		break;
		case "dialIndex":
			targetProp = dialIndex
		break;
		case "dialPattern":
			targetProp = dialPattern
		break;
		case "gemKind":
			targetProp = gemKind	
		break;
		case "jewelMaterial":
			targetProp = jewelMaterial	
		break;
		case "jewelType":
			targetProp = jewelType
		break;
		case "strapMaterial":
			targetProp = strapMaterial
		break;
		case "strapModel":
			targetProp = strapModel
		break;
		case "watchIndication":
			targetProp = watchIndication
		break;
		case "watchMovement":
			targetProp = watchMovement
		break;
		case "watchType":
			targetProp = watchType
		break;

		default:
			targetProp = null
		break;
	}

	if(_chkVal(targetProp)) {
		if(val in targetProp) { //there's an exact match
			res.success = true
			res.data = targetProp[val]
		} else { //there's no match, so let's try one of the synonyms
			
			synonyms.forEach(s => {
				if(type === s['key'] && val === s['val']) {
					res.success = true
					res.data = s['equals']
					messages.push({
						"product":(productNumber ? productNumber : "(no productnumber)"),
						"notification":String(type+" '"+val+"' - alias found: "+s['equals'])
					})
				}
			});

			// if(!res.success) {
			// 	overrides.forEach(s => {
			// 		if(s['attr'] === type && s['of'] === srcProp && s['sku'] === productNumber) {
			// 			res.data = s['equals']
			// 			res.success = true
			// 			messages.push({"product":(productNumber ? productNumber : "(no productnumber)"),"notification":String(type+" '"+val+"' overwritten by "+s['equals'])})
			// 		}
			// 	});

					if(!res.success) {
						console.log(String("Not found: "+productNumber+", "+srcProp+": "+val+" ("+type+")"))
					}
				//}
			//}
		}
	}

	if(res.success) {
		return res.data
	} else {
		messages.push({
			//"product":(productNumber ? productNumber : "(no productnumber)"),
			"problem":String(type+" '"+val+"' not found for "+(srcProp ? srcProp : ""))
		})
		return;
	}
}

const gemKind = {
	"agaat" : "AGATE",
	"amazone" : "AMAZONE",
	"amazoniet" : "AMAZONITE",
	"barnsteen" : "AMBER",
	"amethyst" : "AMETHYST",
	"aquamarijn" : "AQUAMARINE",
	"aventurijn" : "AVENTURINE",
	"beryl" : "BERYLL",
	"bloedkoraal" : "BLOODCORAL",
	"calciet" : "CALCITE",
	"cameo" : "CAMEO",
	"carneool" : "CARNELEAN",
	"cateye" : "CATEYE",
	"keramiek" : "CERAMICS",
	"chalcedoon" : "CHALCEDONY",
	"citrien" : "CITRINE",
	"korund" : "CORUNDUM",
	"kristal" : "CRYSTAL",
	"diamant" : "DIAMOND",
	"smaragd" : "EMERALD",
	"glazuur" : "ENAMEL",
	"epidote" : "EPIDOTE",
	"firepost" : "FIREPOST",
	"fluoriet" : "FLUORITE",
	"granaat" : "GARNET",
	"glas" : "GLASS",
	"heliotroop" : "HELIOTROPE",
	"hematiet" : "HEMATITE",
	"iriskwarts" : "IRISQUARTZ",
	"jade" : "JADE",
	"jargon" : "JARGON",
	"jasper" : "JASPER",
	"labradoriet" : "LABRADORITE",
	"lapis" : "LAPIS",
	"lazuli" : "LAZULI",
	"lazuriet" : "LAZURITE",
	"citroenkwarts" : "LEMONQUARTZ",
	"madeiracitrine" : "MADEIRACITRINE",
	"malachiet" : "MALACHITE",
	"maansteen" : "MOONSTONE",
	"muranoglass" : "MURANOGLASS",
	"natuursteen" : "NATURALSTONE",
	"nefriet" : "NEPHRITE",
	"onyx" : "ONYX",
	"opaal" : "OPAL",
	"parel" : "PEARL",
	"parel akoya" : "PEARLAKOYA",
	"parel cultive" : "PEARLCULTIVE",
	"parel zuidzee" : "PEARLSOUTHSEA",
	"parel zoetwater" : "PEARLSWEETWATER",
	"parel synthetisch" : "PEARLSYNTHETICAL",
	"parel tahiti" : "PEARLTAHITI",
	"peridoot" : "PERIDOTE",
	"fosforiet" : "PHOSPHORITE",
	"quartz" : "QUARTZ",
	"strass" : "RHINESTONE",
	"steenkristal" : "ROCKCRYSTAL",
	"rozenkwarts" : "ROSEQUARTZ",
	"robijn" : "RUBY",
	"saffier" : "SAPPHIRE",
	"leisteen" : "SLATE",
	"rookkwarts" : "SMOKYQUARTZ",
	"soladiet" : "SODALITE",
	"spinel" : "SPINEL",
	"laagsteen" : "STRATUMSTONE",
	"swarovski kristal" : "SWAROVSKICRYSTAL",
	"synthetisch" : "SYNTHETICSTONE",
	"tijgeroog" : "TIGEREYE",
	"topaas" : "TOPAZ",
	"toermalijn" : "TOURMALINE",
	"turquoise" : "TURQUOISE",
	"uvaroviet" : "UVAROVITE",
	"wildvinite" : "WILDVINITE",
	"xylopile" : "XYLOPILE",
	"zirkonia" : "ZIRCONIA"
}

const jewelType = {
	"enkelbandje" : "ANKLE_BRACELET",
	"armband" : "BRACELET",
	"broche" : "BROOCH",
	"bedel" : "CHARM",
	"choker" : "CHOKER",
	"combinatiering" : "COMBINATION_RING",
	"oorringen" : "CREOLE_EARRINGS",
	"manchetknopen" : "CUFFLINK",
	"oorhangers" : "HOOP_EARRINGS",
	"collier/ketting" : "NECKLACE",
	"hanger/bedel/charm" : "PENDANT",
	"ring" : "RING",
	"ring met edelsteen" : "RING_WITH_GEM",
	"ring met parel" : "RING_WITH_PEARL",
	"set" : "SET",
	"bangle armband" : "SLAVE_BRACELET",
	"oorstekers" : "STUD_EARRINGS",
	"tennisarmband" : "TENNIS_BRACELET",
	"tennis ketting" : "TENNIS_NECKLACE"
}

const jewelMaterial = {
	"goud" : "GOLD",
	"zilver" : "SILVER",
	"hout" : "WOOD",
	"canvas / nylon" : "CANVAS",
	"lederen bovenkant" : "UPPER_LEATHER",
    "echt leer": "REAL_LETHER",
	"pu leder" : "PU_LEATHER",
	"metaal" : "METAL",
	"aluminium" : "ALUMINIUM",
	"keramiek" : "CERAMICS",
	"edelstaal" : "STAINLESS_STEEL"
}

const Color = {
	"beige":"BEIGE",
	"zwart":"BLACK",
	"blauw":"BLUE",
	"brons":"BRASS",
	"bruin":"BROWN",
	"creme":"CREME",
	"goudkleurig":"GOLD_COLORED",
	"goud zilver kleurig":"GOLD_SILVER_COLORED",
	"groen":"GREEN",
	"grijs":"GREY",
	"parelmoer":"MOTHER_OF_PEARL",
	"parelmoer gekleurd":"MOTHER_OF_PEARL_COLOURED",
	"oranje":"ORANGE",
	"roze":"PINK",
	"paars":"PURPLE",
	"rood":"RED",
	"roségoudkleurig":"ROSE_GOLD_COLORED",
	"rosé zilver gekleurd":"ROSE_SILVER_COLORED",
	"zilverkleurig":"SILVER_COLORED",
	"taupe":"TAUPE",
	"wit":"WHITE",
	"geel":"YELLOW"
}

const strapMaterial = {
	"aluminium" : "ALUMINIUM",
	"echt leer" : "REAL_LEATHER",
	"lederen bovenkant" : "UPPER_LEATHER",
	"metaal" : "METAL",
	"kunststof / rubber" : "PLASTIC_SILICONE",
	"edelstaal" : "STAINLESS_STEEL",
	"titanium" : "TITANIUM",
	"keramiek" : "CERAMICS",
	"pu leder" : "PU_LEATHER",
	"canvas / nylon" : "CANVAS",
	"echt goud" : "REAL_GOLD"
}

const strapModel = {
	"rekband" : "EXPANDABLE_STRETCH_STRAP",
	"schakelketting zakhorloge" : "SELECT_BAND_FOR_POCKET_WATCH",
	"schakelband" : "SELECT_BAND",
	"milanees / mesh" : "MILANESE_MESH",
	"nato" : "NATO",
	"standaard model" : "STANDARD_MODEL"
}

const caseMaterial = {
	"titanium" : "TITANIUM",
	"edelstaal" : "STAINLESS_STEEL",
	"aluminium" : "ALUMINIUM",
	"metaal" : "METAL",
	"kunststof / rubber" : "PLASTIC_SILICONE",
	"hout" : "WOOD",
	"keramiek" : "CERAMICS"
}


const caseGlasstype = {
	"saffier" : "SAPPHIRE",
	"saffier gecoat" : "SAPPHIRE_COATED",
	"kristal" : "CRYSTAL",
	"mineraal" : "MINERAL",
	"kunststof / rubber" : "SYNTHETIC_PLASTIC",
}

const caseShape = {
	"ovaal" : "OVAL",
	"rechthoek" : "RECTANGLE",
	"rond" : "ROUND",
	"andere vorm" : "OTHER_SHAPE",
	"vierkant" : "SQUARE"
}

const dialIndex = {
	"cijfers (arabisch)" : "ARABIC",
	"puntjes" : "DOTS",
	"romeinse cijfers" : "ROMAN",
	"streepjes" : "STRIPES",
	"geen" : "NONE",
	"sierdiamanten (zirkonia / synthetisch)" : "ORNAMENTAL_DIAMONDS",
	"echte diamanten" : "REAL_DIAMONDS"
}

const dialPattern = {
    "puntjes":"DOTS",
    "streepjes":"STRIPES"
}

const watchIndication = {
	"analoog" : "ANALOG",
	"analoog en digitaal" : "ANALOG_DIGITAL",
	"chrono / multi" : "CHRONO_MULTI",
	"digitaal" : "DIGITAL",
}

const watchMovement = {
	"quartz" : "QUARTZ",
	"solar (zonne energie)" : "SOLAR",
	"gps solar" : "GPS_SOLAR",
	"kinetisch" : "KINETIC",
	"automatisch" : "AUTOMATIC",
	"handopwind" : "HAND_WINDING",
}

const watchType = {
	"smartwatch":"SMARTWATCH",
	"polshorloge":"WRIST",
	"verpleegsterhorloge":"NURSE",
	"zakhorloge":"POCKET"
}

const colorSpellingFix = {
	"Rood":"rode",
	"Zilverkleurig":"zilver",
	"Turquoise":"turquoise",
	"Oranje":"oranje",
	"Zwart":"zwarte",
	"Paars":"paarse",
	"Wit":"witte",
	"Blauw":"blauwe",
	"Roze":"roze",
	"Bruin":"bruine",
	"Grijs":"grijze",
	"Groen":"groene",
	"Champagne":"crèmekleurige"
}

const synonyms = [
	{"key":"jewelMaterial", "val":"gemstones", "equals":""},
    {"key":"jewelMaterial", "val":"all stainless steel", "equals":"STAINLESS_STEEL"},
    {"key":"jewelMaterial", "val":"cotton", "equals":"CANVAS"},
    {"key":"jewelMaterial", "val":"leather", "equals":"REAL_LEATHER"},
    {"key":"jewelMaterial", "val":"paper", "equals":""},
    {"key":"jewelMaterial", "val":"sterling silver 925", "equals":"SILVER"},

    {"key":"Color", "val":"gold", "equals":"GOLD_COLORED"},
    {"key":"Color", "val":"brown", "equals":"BROWN"},
    {"key":"Color", "val":"blue", "equals":"BLUE"},
    {"key":"Color", "val":"black", "equals":"BLACK"},
    {"key":"Color", "val":"grey", "equals":"GREY"},
    {"key":"Color", "val":"green", "equals":"GREEN"},
    {"key":"Color", "val":"mix", "equals":""},
    {"key":"Color", "val":"orange", "equals":"ORANGE"},
    {"key":"Color", "val":"others", "equals":""},
    {"key":"Color", "val":"pink", "equals":"PINK"},
    {"key":"Color", "val":"rosegold", "equals":"ROSE_GOLD_COLORED"},
    {"key":"Color", "val":"red", "equals":"RED"},
    {"key":"Color", "val":"silver", "equals":"SILVER_COLORED"},
    {"key":"Color", "val":"white", "equals":"WHITE"},
    {"key":"Color", "val":"gold", "equals":"GOLD"},
    {"key":"Color", "val":"gold", "equals":"GOLD"},

	{"key":"jewelType", "val":"bracelet", "equals":"BRACELET"},
	{"key":"jewelType", "val":"anklet", "equals":"ANKLE_BRACELET"},
	{"key":"jewelType", "val":"accessoires", "equals":""},
	{"key":"jewelType", "val":"necklace", "equals":"NECKLACE"},
	{"key":"jewelType", "val":"pendant", "equals":"PENDANT"},
	{"key":"jewelType", "val":"ring", "equals":"RING"},

	{"key": "gemKind", "val": "yellow tiger eye", "equals":"TIGEREYE"},
	{"key": "gemKind", "val": "turquoise", "equals":"TURQUOISE"},
	{"key": "gemKind", "val": "steel ball", "equals":""},
	{"key": "gemKind", "val": "howlite white", "equals":""},
	{"key": "gemKind", "val": "ipyg steel ball", "equals":""},
	{"key": "gemKind", "val": "amethyst", "equals":"AMETHYST"},
	{"key": "gemKind", "val": "sodalite blue", "equals":"SODALITE"},
	{"key": "gemKind", "val": "blue lace agate", "equals":"AGATE"},
	{"key": "gemKind", "val": "green aventurism", "equals":"AVENTURINE"},
	{"key": "gemKind", "val": "moss agate", "equals":"AGATE"},
	{"key": "gemKind", "val": "lapis lazuli", "equals":"LAPIS"},
	{"key": "gemKind", "val": "turquoise", "equals":"TURQUOISE"},
	{"key": "gemKind", "val": "rose quartz", "equals":"ROSEQUARTZ"},
	{"key": "gemKind", "val": "argentina hodonite", "equals":""},
	{"key": "gemKind", "val": "green agate", "equals":"AGATE"},
	{"key": "gemKind", "val": "ipyg balls yellow gold plated", "equals":""},
	{"key": "gemKind", "val": "white shell pearl", "equals":"PEARL"},
	{"key": "gemKind", "val": "agalmatolite", "equals":""},
	{"key": "gemKind", "val": "hematite silver", "equals":"HEMATITE"},
	{"key": "gemKind", "val": "stainless steel - rose gold plated", "equals":""},
	{"key": "gemKind", "val": "indian gate", "equals":""},
	{"key": "gemKind", "val": "ipyg balls", "equals":""},
	{"key": "gemKind", "val": "stainless steel - yellow gold plated", "equals":""},
	{"key": "gemKind", "val": "red agate", "equals":"AGATE"},
	{"key": "gemKind", "val": "fossil", "equals":""},
	{"key": "gemKind", "val": "lithographic", "equals":""},
	{"key": "gemKind", "val": "tiger eye yellow", "equals":"TIGEREYE"},
	{"key": "gemKind", "val": "ipyg balls ", "equals":""},
	{"key": "gemKind", "val": "indian agate", "equals": "AGATE"},
	{"key": "gemKind", "val": "turquoise with black", "equals": "TURQUOISE"},
	{"key": "gemKind", "val": "agate black", "equals": "AGATE"},
	{"key": "gemKind", "val": "bright blue agate", "equals": "AGATE"},
	{"key": "gemKind", "val": "tiger eye yellow", "equals": "TIGEREYE"},
	{"key": "gemKind", "val": "red carol-a sardine", "equals": ""},
	{"key": "gemKind", "val": "lignify rodocrosh", "equals": ""},
	{"key": "gemKind", "val": "moss agate", "equals": "AGATE"},
	{"key": "gemKind", "val": "turquoise orange", "equals": "TURQUOISE"},
	{"key": "gemKind", "val": "red aventurine", "equals": "AVENTURINE"},
	{"key": "gemKind", "val": "amazonite", "equals": ""},
	{"key": "gemKind", "val": "sodalite blue", "equals": "SODALITE"},
	{"key": "gemKind", "val": "blackstone", "equals": ""},
	{"key": "gemKind", "val": "lithographic", "equals": ""},
	{"key": "gemKind", "val": "hematite matt", "equals": "HEMATITE"},
	{"key": "gemKind", "val": "hematite silver matt", "equals": "HEMATITE"},
	{"key": "gemKind", "val": "red jasper", "equals": "JASPER"},
	{"key": "gemKind", "val": "green spot stone", "equals": ""},
	{"key": "gemKind", "val": "cherry quartz", "equals": "QUARTZ"},
	{"key": "gemKind", "val": "tiger eye", "equals": "TIGEREYE"},
	{"key": "gemKind", "val": "agate", "equals": "AGATE"},
	{"key": "gemKind", "val": "matt onyx", "equals": "ONYX"},
	{"key": "gemKind", "val": "lava stone black", "equals": ""},
	{"key": "gemKind", "val": "sodalite", "equals": "SODALITE"},
	{"key": "gemKind", "val": "mix color tiger eye r-b-y", "equals": "TIGEREYE"},
	{"key": "gemKind", "val": "tiger eye yellow matt", "equals": "TIGEREYE"},
	{"key": "gemKind", "val": "art. turquoise red", "equals": "TURQUOISE"},
	{"key": "gemKind", "val": "ypyg yellow gold plated balls", "equals": ""},
	{"key": "gemKind", "val": "artificial turquoise red", "equals": "TURQUOISE"},
	{"key": "gemKind", "val": "matt agate", "equals": "AGATE"},
	{"key": "gemKind", "val": "tourmaline", "equals": "TOURMALINE"},
	{"key": "gemKind", "val": "matt black onyx", "equals": "ONYX"},
	{"key": "gemKind", "val": "labradorite", "equals": "LABRADORITE"},
	{"key": "gemKind", "val": "bronzite", "equals": ""},
	{"key": "gemKind", "val": "rhyolite", "equals": ""},
	{"key": "gemKind", "val": "agate red matt", "equals": "AGATE"},
	{"key": "gemKind", "val": "larvakite dark grey", "equals": ""},
	{"key": "gemKind", "val": "composite with malachite", "equals": "MALACHITE"},
	{"key": "gemKind", "val": "sodalite blue matt", "equals": "SODALITE"},
	{"key": "gemKind", "val": "ipyg o5mm", "equals": ""},
	{"key": "gemKind", "val": "indian agate (rose/brown only)", "equals": "AGATE"},
	{"key": "gemKind", "val": "larvikite grey", "equals": ""},
	{"key": "gemKind", "val": "onyx matt", "equals": "ONYX"},
	{"key": "gemKind", "val": "mad onyx", "equals": "ONYX"},
	{"key": "gemKind", "val": "composite with malachite azurit", "equals": "MALACHITE"},
	{"key": "gemKind", "val": "sesame red", "equals": ""},
	{"key": "gemKind", "val": "matt onyx ", "equals": "ONYX"},
	{"key": "gemKind", "val": "sodalite blue and d.blue tiger eye", "equals": "SODALITE"},
	{"key": "gemKind", "val": "tiger eye blue", "equals": "TIGEREYE"},
	{"key": "gemKind", "val": "larvikite grey matt", "equals": ""},
	{"key": "gemKind", "val": "black onyx", "equals": "ONYX"},
	{"key": "gemKind", "val": "onyx matt and mixed tiger eye", "equals": "ONYX"},
	{"key": "gemKind", "val": "hematite grey", "equals": "HEMATITE"},
	{"key": "gemKind", "val": "blue spot stone", "equals": ""},
	{"key": "gemKind", "val": "grain stone", "equals": ""},
	{"key": "gemKind", "val": "brecciated jasper matt", "equals": "JASPER"},
	{"key": "gemKind", "val": "brecciated jasper", "equals": "JASPER"},
	{"key": "gemKind", "val": "snowflake", "equals": ""},
	{"key": "gemKind", "val": "green lace stone and moss agate", "equals": "AGATE"},
	{"key": "gemKind", "val": "turquoise with black matt", "equals": "TURQUOISE"},
	{"key": "gemKind", "val": "black network", "equals": ""},
	{"key": "gemKind", "val": "tiger eye bordeaux", "equals": "TIGEREYE"},
	{"key": "gemKind", "val": "ebony wood", "equals": ""},
	{"key": "gemKind", "val": "turquois", "equals": "TURQUOISE"},
	{"key": "gemKind", "val": "pink tophus", "equals": ""},
	{"key": "gemKind", "val": "green spot", "equals": ""},
	{"key": "gemKind", "val": "white tophus", "equals": ""},
	{"key": "gemKind", "val": "argentina rhodonite", "equals": ""},
	{"key": "gemKind", "val": "blue jade", "equals": "JADE"},
	{"key": "gemKind", "val": "lignify rhodocrosite", "equals": ""},
	{"key": "gemKind", "val": "bronzite brown", "equals": ""},
	{"key": "gemKind", "val": "brecc.jasper", "equals": "JASPER"},
	{"key": "gemKind", "val": "lithographic and yellow tiger eye", "equals": "TIGEREYE"},
	{"key": "gemKind", "val": "beige jaspe", "equals": "JASPER"},
	{"key": "gemKind", "val": "ss balls", "equals": ""},
	{"key": "gemKind", "val": "iprg balls", "equals": ""},
	{"key": "gemKind", "val": "larvikite", "equals": ""},
	{"key": "gemKind", "val": "red tiger eye bordeaux", "equals": "TIGEREYE"},
	{"key": "gemKind", "val": "labradorite shiny", "equals": "LABRADORITE"},
	{"key": "gemKind", "val": "han white jade", "equals": "JADE"},
	{"key": "gemKind", "val": "matt larvikite grey", "equals": ""},
	{"key": "gemKind", "val": "lapis lazuli blue", "equals": "LAPIS"},
	{"key": "gemKind", "val": "chrysanthemum and agate black", "equals": "AGATE"},
	{"key": "gemKind", "val": "art.malachite", "equals": "MALACHITE"},
	{"key": "gemKind", "val": "silver balls", "equals": ""},
	{"key": "gemKind", "val": "ypyg o5mm", "equals": ""},
	{"key": "gemKind", "val": "with 5 pcs. green agate", "equals": "AGATE"},
	{"key": "gemKind", "val": "blue lace agate and sodalite blue", "equals": "AGATE"},
	{"key": "gemKind", "val": "ypyg yellow gold plated ball", "equals": ""},
	{"key": "gemKind", "val": "silver ball", "equals": ""},
	{"key": "gemKind", "val": "alabaster", "equals": ""},
	{"key": "gemKind", "val": "sardonyx", "equals": ""},
	{"key": "gemKind", "val": "l.blue jade", "equals": "JADE"}
]

function init(productData) {
	
	const res = rrParser(productData)

	let now = String(Math.floor(Date.now() / 1000))

   //Save the results to a JSON-file
	if(res && res.data && res.data.length > 0 && process.argv[2] === "--save") {
		
		fs.writeFile("./output/"+now+".json", JSON.stringify(res.data,null,2), function(err) {if(err) { throw new Error(err); }});
		console.log('result saved to /output')

		if(res.problems) {
			fs.writeFile("./output/"+now+"_problems.json", JSON.stringify(res.problems,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('problems saved to /output')
		}

		if(res.warnings) {
			fs.writeFile("./output/"+now+"_warnings.json", JSON.stringify(res.warnings,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('warnings saved to /output')

		}

		if(res.notifications) {
			fs.writeFile("./output/"+now+"_notifications.json", JSON.stringify(res.notifications,null,2), function(err) {if(err) { throw new Error(err); }});
			console.log('notifications saved to /output')

		}
	}
}